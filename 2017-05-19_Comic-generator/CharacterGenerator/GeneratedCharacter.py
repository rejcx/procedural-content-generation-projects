import pygame, sys
from pygame.locals import *
import random
import os

class GeneratedCharacter:
    def __init__( self ):
        self.Setup()

    def Original( self, bodyParts ):
        self.Generate( bodyParts )

    def Copy( self, existingCharacter ):
        self.headIndex = existingCharacter.headIndex
        self.torsoIndex = existingCharacter.torsoIndex
        self.handIndex = existingCharacter.handIndex
        #self.earIndex = existingCharacter.earIndex
        self.backhairIndex = existingCharacter.backhairIndex
        self.fronthairIndex = existingCharacter.fronthairIndex
        #self.hatIndex = existingCharacter.hatIndex
        self.eyeIndex = existingCharacter.eyeIndex
        self.mouthIndex = existingCharacter.mouthIndex

    def Setup( self ):
        self.headIndex = 0
        self.torsoIndex = 0
        self.handIndex = 0
        #self.earIndex = 0
        self.backhairIndex = 0
        self.fronthairIndex = 0
        #self.hatIndex = 0
        self.eyeIndex = 0
        self.mouthIndex = 0
        self.position = ( 0, 0 )



    def SetPosition( self, x, y ):
        self.position = ( x, y )


    def Generate( self, bodyParts ):
        self.SelectHead( bodyParts["heads"] )
        self.SelectTorso( bodyParts["torsos"] )
        self.SelectHands( bodyParts["hands"] )
        #self.SelectEars( bodyParts["ears"] )
        self.SelectHair( bodyParts["backhair"], bodyParts["fronthair"] )
        #self.SelectHat( bodyParts["hats"] )
        self.SelectEyes( bodyParts["eyes"] )
        self.SelectMouth( bodyParts["mouths"] )

        print( "Selected..." )
        print( "\t Head: " + str( self.headIndex ) +
            ", Torso: " + str( self.torsoIndex ) +
            ", Hands: " + str( self.handIndex ) +
            #", Ears: " + str( self.earIndex ) +
            ", Back Hair: " + str( self.backhairIndex ) +
            ", Front Hair: " + str( self.fronthairIndex ) +
            #", Hat: " + str( self.hatIndex ) +
            ", Eyes: " + str( self.eyeIndex ) +
            ", Mouth: " + str( self.mouthIndex ) )

    def ChangeExpression( self, bodyParts ):
        self.SelectHands( bodyParts["hands"] )
        self.SelectEyes( bodyParts["eyes"] )
        self.SelectMouth( bodyParts["mouths"] )

    def SelectHead( self, parts ):
        self.headIndex = random.randint( 0, len( parts ) - 1 )
        
    def SelectTorso( self, parts ):
        self.torsoIndex = random.randint( 0, len( parts ) - 1 )
        
    def SelectHands( self, parts ):
        self.handIndex = random.randint( 0, len( parts ) - 1 )
        
    def SelectEars( self, parts ):
        self.earIndex = random.randint( 0, len( parts ) - 1 )
        
    def SelectHair( self, backParts, frontParts ):
        # len(backParts) should be the same as len(frontParts).
        self.backhairIndex = random.randint( 0, len( backParts ) - 1 )
        self.fronthairIndex = self.backhairIndex
        
    def SelectHat( self, parts ):
        self.hatIndex = random.randint( 0, len( parts ) - 1 )
        
    def SelectEyes( self, parts ):
        self.eyeIndex = random.randint( 0, len( parts ) - 1 )
        
    def SelectMouth( self, parts ):
        self.mouthIndex = random.randint( 0, len( parts ) - 1 )


    def Draw( self, screen, bodyParts ):

        # Draw in every frame
        screen.blit( bodyParts[ "backhair" ][ self.backhairIndex ],    self.position ) # hair behind everything
        screen.blit( bodyParts[ "hands" ][ self.handIndex ],   self.position ) # hands behind torso
        screen.blit( bodyParts[ "torsos" ][ self.torsoIndex ], self.position )
        screen.blit( bodyParts[ "heads" ][ self.headIndex ],   self.position )
        screen.blit( bodyParts[ "fronthair" ][ self.fronthairIndex ],    self.position ) # hair in front of head
        #screen.blit( bodyParts[ "ears" ][ self.earIndex ],     self.position )
        #screen.blit( bodyParts[ "hats" ][ self.hatIndex ],     self.position )
        screen.blit( bodyParts[ "eyes" ][ self.eyeIndex ],     self.position )
        screen.blit( bodyParts[ "mouths" ][ self.mouthIndex ], self.position )



