import pygame, sys
import random
from pygame.locals import *
import api_info
import tweepy, time, sys
import BeautifulSoup
import urllib2
import copy

esperantoComic = False

pygame.init()
pygame.font.init()
fps = pygame.time.Clock()

window = pygame.display.set_mode( ( 400*2+60, 440 ) )
pygame.display.set_caption( "Random comic!" )

niceColors = [ (255, 120, 120), (255, 60, 100), (255, 160, 0), (100, 170, 0), (100, 160, 210), (100, 100, 210) ]

bottomY = 420

font = pygame.font.Font( "Redkost Comic.otf", 20 )
smallfont = pygame.font.Font( "Redkost Comic.otf", 12 )

#topics = [ "Esperanto", "Ido", "Laadan", "conlangs", "Lojban", "Toki Pona", "Interlingua", "Volapuk", "language", "constructed language" ]
topics = []

# 180 calls every 15 minutes
timeInterval = 15 * 60
delayInterval = ( timeInterval / 180 ) * 1000

class Character:
    def __init__( self, x, y, lookat_x, lookat_y ):
        self.color = None
        self.x = x
        self.y = y
        self.body_x = 0
        self.body_y = 0
        self.body_width = 0
        self.body_height = 0
        self.head_x = 0
        self.head_y = 0
        self.head_width = 0
        self.head_height = 0
        self.eye_width = 20
        self.eye_distance = 0
        self.left_eye_x = 0
        self.eye_y = 0
        self.right_eye_x = 0
        self.lookat_x = lookat_x
        self.lookat_y = lookat_y
        self.type = ""
        self.mouth_length = 0

        self.Generate()

    def Draw( self ):
        # Draw body
        pygame.draw.rect( window, self.color, ( self.body_x, self.body_y + self.body_height/2, self.body_width, self.body_height / 2 ) ) # color
        #pygame.draw.rect( window, pygame.Color( 0, 0, 0 ), ( 0, self.body_height/2, self.body_width, self.body_height / 2 ), 1 ) # outline
        
        pygame.draw.ellipse( window, self.color, ( self.body_x, self.body_y, self.body_width, self.body_height ), 0 ) # color
        #pygame.draw.ellipse( window, pygame.Color( 0, 0, 0 ), ( 0, 0, self.body_width, self.body_height ), 1 ) # outline

        # Draw head
        pygame.draw.ellipse( window, self.color, ( self.head_x, self.head_y, self.head_width, self.head_height ), 0 ) # color

        # Draw ears
        if ( self.type == "cat" ):
            # pointy ears
            pygame.draw.polygon( window, self.color, [ [self.x, self.y + self.head_height / 2], [self.x, self.y - 10], [self.x + self.head_width, self.y + self.head_height / 2] ], 0 )
            pygame.draw.polygon( window, self.color, [ [self.x + self.head_width - self.head_width, self.y + self.head_height / 2], [self.x + self.head_width, self.y + self.head_height / 2], [self.x + self.head_width, self.y - 10] ], 0 )

        elif ( self.type == "pig" ):
            pygame.draw.ellipse( window, self.color, ( self.x - 10, self.y, 50, 25 ), 0 )
            pygame.draw.ellipse( window, self.color, ( self.x + self.head_width + 10 - 50, self.y, 50, 25 ), 0 )
            
        elif ( self.type == "bear" ):
            pygame.draw.ellipse( window, self.color, ( self.x - 10, self.y - 10, 50, 50 ), 0 )
            pygame.draw.ellipse( window, self.color, ( self.x + self.head_width + 10 - 50, self.y - 10, 50, 50 ), 0 )

        # Mouth
        if ( self.type == "cat" or self.type == "bear" ):
            pygame.draw.line( window, pygame.Color( 0, 0, 0 ), [ self.x + self.head_width/2 - self.mouth_length, self.y + self.head_height * 4/6 ], [ self.x + self.head_width/2 + self.mouth_length, self.y + self.head_height * 4/6 ], 2 )
            
        elif ( self.type == "bird" ): #beak
            pygame.draw.polygon( window, pygame.Color( 255, 255, 0 ), [ [ self.x + 10, self.y + self.head_height * 4/6], [self.x + self.head_width - 10, self.y + self.head_height * 4/6], [self.x + self.head_width / 2, self.y + self.head_height * 5/6] ], 0 )

        elif ( self.type == "pig" ):
            snout_width = self.head_width / 2
            snout_x = self.head_width / 2 - snout_width / 2 + self.x
            head_middle = self.head_x + self.head_width / 2
            pygame.draw.ellipse( window, pygame.Color( 0, 0, 0 ), ( snout_x, self.head_y + self.head_height * 4/6, snout_width, 20 ), 2 )
            pygame.draw.ellipse( window, pygame.Color( 0, 0, 0 ), ( head_middle - 7, self.head_y + self.head_height * 4/6 + 10, 5, 5 ), 0 )
            pygame.draw.ellipse( window, pygame.Color( 0, 0, 0 ), ( head_middle + 5, self.head_y + self.head_height * 4/6 + 10, 5, 5 ), 0 )

        # Draw eyes
        pygame.draw.ellipse( window, pygame.Color( 255, 255, 255 ), ( self.x + self.left_eye_x, self.eye_y, self.eye_width, self.eye_width ), 0 )
        pygame.draw.ellipse( window, pygame.Color( 255, 255, 255 ), ( self.x + self.right_eye_x, self.eye_y, self.eye_width, self.eye_width ), 0 )
        
        # Pupils
        plx = self.x + self.left_eye_x + self.eye_width / 2 - 2
        prx = self.x + self.right_eye_x + self.eye_width / 2 - 2
        py = self.eye_y + self.eye_width / 2 - 2

        if ( self.lookat_x < self.x ):
            plx = plx - self.eye_width / 2
            prx = prx - self.eye_width / 2
        elif ( self.lookat_x > self.x ):
            plx = plx + self.eye_width / 2
            prx = prx + self.eye_width / 2            
        
        pygame.draw.ellipse( window, pygame.Color( 0, 0, 0 ), ( plx, py, 5, 5 ), 0 )
        pygame.draw.ellipse( window, pygame.Color( 0, 0, 0 ), ( prx, py, 5, 5 ), 0 )

    def Generate( self ):
        randColor = random.randint( 0, len( niceColors )-1 )
        self.color = pygame.Color( niceColors[ randColor ][0], niceColors[ randColor ][1], niceColors[ randColor ][2] )
        self.GenerateHead()
        self.GenerateBody()
        
        # Reposition...
        actualY = bottomY - self.head_height - self.body_height
        print( actualY )
        diff = actualY - self.y
        self.y = self.y + diff
        self.head_y = self.head_y + diff
        self.body_y = self.body_y + diff

        t = random.randint( 0, 3 )

        self.GenerateEyes()

        if ( t == 0 ):
            self.type = "cat"
            self.mouth_length = random.randint( 2, self.head_width / 2 )
        elif ( t == 1 ):
            self.type = "bird"
        elif ( t == 2 ):
            self.type = "pig"
        elif ( t == 3 ):
            self.type = "bear"

        print( self.x, self.y, self.head_x, self.head_y, self.left_eye_x, self.right_eye_x )

    def GenerateEyes( self ):
        self.eye_width = random.randint( 15, 20 )
        self.left_eye_x = random.randint( 0, self.head_width / 2 - self.eye_width - 5 )
        self.right_eye_x = self.head_width - self.left_eye_x - self.eye_width
        self.eye_y = self.head_y + (self.head_height / 3)

    def GenerateHead( self ):
        self.head_width = random.randint( 50, 100 )
        self.head_height = random.randint( 50, 100 )
        self.head_x = self.x
        self.head_y = self.y

    def GenerateBody( self ):
        self.body_width = random.randint( 50, 100 )
        self.body_height = 100
        self.body_x = self.x + self.head_width / 2 - self.body_width / 2
        self.body_y = self.y + self.head_height

# FROM http://pygame.org/wiki/TextWrap


def LoadTopics( filename ):
    global topics
    with open( filename ) as fileHandler:
        lines = fileHandler.readlines()

    for line in lines:
        topics.append( line.strip( "\n" ) )

    print( str( len( topics ) ) + " topics" )



def LoadEsperantoTopics( filename ):
    # Parse espdic
    global topics
    with open( filename ) as fileHandler:
        lines = fileHandler.readlines()

    count = 0
    for line in lines:
        if ( count == 0 ):
            count = count + 1
            continue;
        #colonIndex = line.index( ":" )
        #esperanto = line[:colonIndex]
        esperanto = line.strip()
        topics.append( esperanto )

    print( str( len( topics ) ) + " topics" )



    
    
def drawText(surface, text, color, rect, font, aa=False, bkg=None):
    rect = Rect(rect)
    y = rect.top
    lineSpacing = -2

    # get the height of the font
    fontHeight = font.size("Tg")[1]

    while text:
        i = 1

        # determine if the row of text will be outside our area
        if y + fontHeight > rect.bottom:
            break

        # determine maximum width of line
        while font.size(text[:i])[0] < rect.width and i < len(text):
            i += 1

        # if we've wrapped the text, then adjust the wrap to the last word      
        if i < len(text): 
            i = text.rfind(" ", 0, i) + 1

        # render the line and blit it to the surface
        if bkg:
            image = font.render(text[:i], 1, color, bkg)
            image.set_colorkey(bkg)
        else:
            image = font.render(text[:i], aa, color)

        surface.blit(image, (rect.left, y))
        y += fontHeight + lineSpacing

        # remove the text we just blitted
        text = text[i:]

    return text











api = None

def InitTwitterApi():
    print( "Init Twitter API" )
    global api
    auth = tweepy.OAuthHandler( api_info.consumerKey, api_info.consumerSecret )
    auth.set_access_token( api_info.accessKey, api_info.accessSecret )
    api = tweepy.API( auth )

def GetTopic():
    topic = topics[ random.randint( 0, len( topics ) - 1 ) ]
    return topic
    
    # https://en.wikipedia.org/wiki/Special:Random
    #soup = BeautifulSoup.BeautifulSoup( urllib2.urlopen('https://en.wikipedia.org/wiki/Special:Random') )
    # Wikipedia isn't a great source; not much tweets about stuff

    # What is the topic?
    #title = soup.title.string.replace( " - Wikipedia", "" )

    # return title

def GetResponseTopic( statement ):
    # Look for a noun that is contained in the original tweet
    words = statement.split( " " )
    foundWord = ""
    
    for word in words:
        if word in topics:
            foundWord = word
            break

    if ( foundWord == "" ):
        return GetTopic()
    
    return foundWord

    

generating = True

def GetText( topic = "" ):
    global generating

    original = topic
    
    generating = True
    #API.search

    if ( original == "" ):
        topic = GetTopic()

    language = "en"

    if ( esperantoComic == True ):
        language = "eo"
    
    global api
    results = api.search( topic, language, language, 1 )

    while( len( results ) == 0 or "@" in results[0].text or "http" in results[0].text or "#" in results[0].text ):
        if ( original == "" ):
            topic = GetTopic()
        else:
            # Trying to find a topic based on this word
            pygame.time.delay( delayInterval ) # Just wait
            
        print( "New topic:", topic )
        results = api.search( topic, language, language, 1 )
        pygame.time.delay( delayInterval ) # don't bug Twitter too often.

    generating = False
    
    if ( len( results ) > 0 ):
        return results[0].text, "@" + results[0].author.screen_name
    else:
        return "", ""



def CreateCharacters():
    global chara
    global charb
    global charc
    global chard
    
    chara.Generate()
    charb.Generate()
    charc = copy.deepcopy( chara )
    chard = copy.deepcopy( charb )

    charc.x = charc.x + 400
    charc.head_x = charc.head_x + 400
    charc.body_x = charc.body_x + 400
    chard.x = chard.x + 400
    chard.head_x = chard.head_x + 400
    chard.body_x = chard.body_x + 400

    
InitTwitterApi()

chara = Character( 100, 100, 200, 100 )
charb = Character( 300, 100, 100, 100 )
charc = None
chard = None

CreateCharacters()

text = None
author = None

if ( esperantoComic == True ):
    LoadEsperantoTopics( "espdic.txt" )
else:
    LoadTopics( "nounlist.txt" )

#topic = topics[ random.randint( 0, len( topics ) - 1 ) ]


background = ( random.randint( 200, 255 ), random.randint( 200, 255 ), random.randint( 200, 255 ) )

text, author = GetText()

topicB = GetResponseTopic( text )
text2, author2 = GetText( topicB )

while True:
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()
            
        elif ( event.type == KEYDOWN ):
            if ( event.key == K_r ):
                background = ( random.randint( 200, 255 ), random.randint( 200, 255 ), random.randint( 200, 255 ) )

                CreateCharacters()
                
                text, author = GetText(  )
                text2, author2 = GetText(  )

                window.fill( pygame.Color( 0, 0, 0 ) )
                pygame.display.update()
                
            elif ( event.key == K_a ):
                text, author = GetText(  )

                window.fill( pygame.Color( 0, 0, 0 ) )
                pygame.display.update()
                
            elif ( event.key == K_b ):
                text2, author2 = GetText(  )

                window.fill( pygame.Color( 0, 0, 0 ) )
                pygame.display.update()

        window.fill( pygame.Color( 0, 0, 0 ) )

        if ( generating == False ):
            # draw frames
            pygame.draw.rect( window, background, ( 20, 20, 400, 400 ), 0 )
            pygame.draw.rect( window, background, ( 440, 20, 400, 400 ), 0 )
            
            chara.Draw()
            charb.Draw()
            charc.Draw()
            chard.Draw()
            
            if ( text is not None ):
                # draw word balloon
                pygame.draw.rect( window, pygame.Color( 255, 255, 255 ), ( 20, 20, 400, 150 ), 0 )
                pygame.draw.polygon( window, pygame.Color( 255, 255, 255 ), [ [150, 100], [150, 220], [250, 100] ], 0 )
                
                pygame.draw.rect( window, pygame.Color( 255, 255, 255 ), ( 440, 20, 400, 150 ), 0 )
                pygame.draw.polygon( window, pygame.Color( 255, 255, 255 ), [ [750, 100], [750, 220], [650, 100] ], 0 )
                
                drawText( window, text, pygame.Color( 0, 0, 0 ), ( 40, 40, 360, 150 ), font )
                drawText( window, author, pygame.Color( 200, 200, 200 ), ( 40, 150, 360, 150 ), smallfont )
                
                drawText( window, text2, pygame.Color( 0, 0, 0 ), ( 460, 40, 360, 150 ), font )
                drawText( window, author2, pygame.Color( 200, 200, 200 ), ( 460, 150, 360, 150 ), smallfont )

        pygame.display.update()

    
    fps.tick( 30 )
