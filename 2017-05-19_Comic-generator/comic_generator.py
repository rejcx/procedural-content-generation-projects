import pygame, sys
from pygame.locals import *
import random
import os

from CharacterGenerator import GeneratedCharacter
from DialogGenerator import DialogGenerator

def LoadImages( bodyParts ):
    print( "Load images" )
    rootFolder = os.path.join( "images", os.path.dirname( os.path.realpath( __file__ ) ) )
    
    contents = list()

    for root, directories, files in os.walk( rootFolder, topdown = False ):
        for name in files:
            print( "File: " + name )
            contents.append( os.path.join( root, name ) )

    for item in contents:

        # Skip if it isn't an image
        if ( "png" not in item ):
            continue
            
        # Load the image
        surface = pygame.image.load( os.path.join( rootFolder, item ) )

        # Throw in the list
        if      ( "ears" in item ):
            bodyParts["ears"].append( surface )
            
        elif    ( "eyes" in item ):
            bodyParts["eyes"].append( surface )
            
        elif    ( "hair_back" in item ):
            bodyParts["backhair"].append( surface )
            
        elif    ( "hair_front" in item ):
            bodyParts["fronthair"].append( surface )
            
        elif    ( "hands" in item ):
            bodyParts["hands"].append( surface )
            
        elif    ( "hat" in item ):
            bodyParts["hats"].append( surface )
            
        elif    ( "head" in item ):
            bodyParts["heads"].append( surface )
            
        elif    ( "mouth" in item ):
            bodyParts["mouths"].append( surface )
            
        elif    ( "torso" in item ):
            bodyParts["torsos"].append( surface )
    
    print( str( len( contents ) ) + " total images... \n\t " +  
        str( len( bodyParts["ears"] ) ) + " ears, " +
        str( len( bodyParts["eyes"] ) ) + " eyes, " +
        str( len( bodyParts["backhair"] ) ) + " back hair, " +
        str( len( bodyParts["fronthair"] ) ) + " front hair, " +
        str( len( bodyParts["hands"] ) ) + " hands, " +
        str( len( bodyParts["hats"] ) ) + " hats, " +
        str( len( bodyParts["heads"] ) ) + " heads, " +
        str( len( bodyParts["mouths"] ) ) + " mouths, " +
        str( len( bodyParts["torsos"] ) ) + " torsos" )


def SetupCharacters( bodyParts, characterA1, characterA2, characterB1, characterB2 ):
    characterA1.SetPosition( 10, 110 )
    characterA1.Original( bodyParts )

    characterA2.Copy( characterA1 )
    characterA2.SetPosition( 10+frameOffset, 110 )
    characterA2.ChangeExpression( bodyParts )

    characterB1.SetPosition( 210, 110 )
    characterB1.Original( bodyParts )

    characterB2.Copy( characterB1 )
    characterB2.SetPosition( 210+frameOffset, 110 )
    characterB2.ChangeExpression( bodyParts )





def drawText(surface, text, color, rect, font, aa=False, bkg=None):
    rect = Rect(rect)
    y = rect.top
    lineSpacing = -2

    # get the height of the font
    fontHeight = font.size("Tg")[1]

    while text:
        i = 1

        # determine if the row of text will be outside our area
        if y + fontHeight > rect.bottom:
            break

        # determine maximum width of line
        while font.size(text[:i])[0] < rect.width and i < len(text):
            i += 1

        # if we've wrapped the text, then adjust the wrap to the last word      
        if i < len(text): 
            i = text.rfind(" ", 0, i) + 1

        # render the line and blit it to the surface
        if bkg:
            image = font.render(text[:i], 1, color, bkg)
            image.set_colorkey(bkg)
        else:
            image = font.render(text[:i], aa, color)

        surface.blit(image, (rect.left, y))
        y += fontHeight + lineSpacing

        # remove the text we just blitted
        text = text[i:]

    return text







window = None
fps = None
background = None
font = None

def PygameInit():
    global window
    global fps
    global background
    global font
    
    pygame.init()
    pygame.font.init()

    fps = pygame.time.Clock()

    window = pygame.display.set_mode( ( 400*2+60, 440 ) )
    pygame.display.set_caption( "Random comic!" )
    
    background = pygame.Color( random.randint( 100, 200 ), random.randint( 100, 200 ), random.randint( 100, 200 ) )
    
    font = pygame.font.Font( "Redkost Comic.otf", 20 )
    

def PygameLoop( bodyParts, characters, dialog ):
    global window
    global fps
    global background
    global font

    text1, text2 = dialog.GetDialog()
    
    
    while True:
        for event in pygame.event.get():
            if ( event.type == QUIT ):
                pygame.quit()
                sys.exit()
                
            elif ( event.type == KEYDOWN ):
                if ( event.key == K_c ):
                    print( "Regenerate characters" )
                    SetupCharacters( bodyParts, characters[0], characters[1], characters[2], characters[3] )

                elif ( event.key == K_d ):
                    print( "Regenerate text" )
                    text1, text2 = dialog.GetDialog()

                elif ( event.key == K_a ):
                    print( "Regenerate all" )
                    background = pygame.Color( random.randint( 100, 200 ), random.randint( 100, 200 ), random.randint( 100, 200 ) )
    
                    SetupCharacters( bodyParts, characters[0], characters[1], characters[2], characters[3] )

                    text1, text2 = dialog.GetDialog()

        # Backgrounds
        window.fill( pygame.Color( 0, 0, 0 ) )

        pygame.draw.rect( window, background, ( 20, 20, 400, 400 ), 0 )
        pygame.draw.rect( window, background, ( 440, 20, 400, 400 ), 0 )

        for character in characters:        
            character.Draw( window, bodyParts )



        # draw word balloons
        ballonHeight = 100
        pygame.draw.rect( window, pygame.Color( 255, 255, 255 ), ( 20, 20, 400, ballonHeight ), 0 )
        pygame.draw.polygon( window, pygame.Color( 255, 255, 255 ), [ [150, ballonHeight], [150, ballonHeight + 50], [250, ballonHeight] ], 0 )
        
        pygame.draw.rect( window, pygame.Color( 255, 255, 255 ), ( 440, 20, 400, ballonHeight ), 0 )
        pygame.draw.polygon( window, pygame.Color( 255, 255, 255 ), [ [750, ballonHeight], [750, ballonHeight + 50], [650, ballonHeight] ], 0 )

        # Draw dialog
        drawText( window, text1, pygame.Color( 0, 0, 0 ), ( 40, 40, 360, 150 ), font )
        drawText( window, text2, pygame.Color( 0, 0, 0 ), ( 460, 40, 360, 150 ), font )
        
        pygame.display.update()
        fps.tick( 30 )

# Program ---------------------------

bodyParts = {}
bodyParts["heads"]  = []
bodyParts["torsos"] = []
bodyParts["hands"]  = []
bodyParts["ears"]   = []
bodyParts["fronthair"]   = []
bodyParts["backhair"]   = []
bodyParts["hats"]   = []
bodyParts["eyes"]   = []
bodyParts["mouths"] = []

PygameInit()
LoadImages( bodyParts )

frameOffset = 440 - 20
    
characterA1 = GeneratedCharacter()
characterA2 = GeneratedCharacter()
characterB1 = GeneratedCharacter()
characterB2 = GeneratedCharacter()

SetupCharacters( bodyParts, characterA1, characterA2, characterB1, characterB2 )

dialog = DialogGenerator()

PygameLoop( bodyParts, [ characterA1, characterA2, characterB1, characterB2 ], dialog )
