myFile = open( "red.ppm", "w" ) # open red.ppm, to (w)rite to. We access this file via the variable myFile.

# Variables to keep track of width/height
width = 300
height = 250

# Write out header
# \n means new-line
myFile.write( "P3 \n" )
# output image dimensions
myFile.write( str( width ) + " " + str( height ) + " \n" )
# keep it at 255 colors
myFile.write( "255 \n" )

# Draw out colors, each pixel has 3 values: RED GREEN BLUE
# Each color can be between 0 and 255.
# 0 0 0 = black,    255 255 255 = white,
# 255 0 0 = red,    0 255 0 = green,    0 0 255 = blue
for y in range( 0, height ):
    for x in range( 0, width ):
        myFile.write( "255 0 0 \n" )  # RED GREEN BLUE

myFile.close()
