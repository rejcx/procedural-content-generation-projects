myFile = open( "draw.ppm", "w" ) # open red.ppm, to (w)rite to. We access this file via the variable myFile.

# Variables to keep track of width/height
width = 5
height = 5

# Write out header
# \n means new-line
myFile.write( "P3 \n" )
# output image dimensions
myFile.write( str( width ) + " " + str( height ) + " \n" )
# keep it at 255 colors
myFile.write( "255 \n" )

# Draw pixels out individually!
myFile.write( "255 0 0 \n" )    # pixel 1   Red
myFile.write( "255 0 0 \n" )    # pixel 2   Red
myFile.write( "255 0 0 \n" )    # pixel 3   Red
myFile.write( "0 255 0 \n" )    # pixel 4   Green
myFile.write( "0 255 0 \n" )    # pixel 5   Green
myFile.write( "0 255 0 \n" )    # pixel 6   Green
myFile.write( "0 0 255 \n" )    # pixel 7   Blue
myFile.write( "0 0 255 \n" )    # pixel 8   Blue
myFile.write( "0 0 255 \n" )    # pixel 9   Blue
myFile.write( "255 0 255 \n" )  # pixel 10  Red + Blue = Magenta
myFile.write( "255 255 0 \n" )  # pixel 11  Red + Green = Yellow
myFile.write( "0 255 255 \n" )  # pixel 12  Green + Blue = Cyan
myFile.write( "255 255 255 \n" )    # pixel 13  White
myFile.write( "0 0 0 \n" )          # pixel 14  Black
myFile.write( "100 0 0 \n" )        # pixel 15  Dark red
myFile.write( "0 100 0 \n" )        # pixel 16  Dark green
myFile.write( "0 0 100 \n" )        # pixel 17  Dark blue
myFile.write( "100 0 100 \n" )        # pixel 18  Dark magenta
myFile.write( "100 100 0 \n" )        # pixel 19  Dark yellow
myFile.write( "0 100 100 \n" )        # pixel 20  Dark cyan
myFile.write( "200 200 200 \n" )        # pixel 21  Light gray
myFile.write( "100 100 100 \n" )        # pixel 22  Dark gray
myFile.write( "200 200 200 \n" )        # pixel 23  Light gray
myFile.write( "100 100 100 \n" )        # pixel 24  Dark gray
myFile.write( "255 0 0 \n" )        # pixel 25  Red



myFile.close()
