import random

myFile = open( "random2.ppm", "w" ) # open red.ppm, to (w)rite to. We access this file via the variable myFile.

# Variables to keep track of width/height
width = 300
height = 250

# Write out header
# \n means new-line
myFile.write( "P3 \n" )
# output image dimensions
myFile.write( str( width ) + " " + str( height ) + " \n" )
# keep it at 255 colors
myFile.write( "255 \n" )

# Decide the two colors to use ahead of time! Random background / foreground
bgColor = str( random.randrange( 0, 255 ) ) + " " + str( random.randrange( 0, 255 ) ) + " " + str( random.randrange( 0, 255 ) )
fgColor = str( random.randrange( 0, 255 ) ) + " " + str( random.randrange( 0, 255 ) ) + " " + str( random.randrange( 0, 255 ) )

for y in range( 0, height ):
    for x in range( 0, width ):
        # Randomly decide whether to draw the background or foreground
        choice = random.randrange( 0, 10 )

        if ( choice < 3 ):
                myFile.write( fgColor + "\n" )
        else:
                myFile.write( bgColor + "\n" )

myFile.close()
