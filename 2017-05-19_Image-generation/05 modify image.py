import random

inputFile = open( "example.ppm", "r" ) # open to read
outputFile = open( "distort.ppm", "w" ) # open to write

# READ HEADER
inputContents = inputFile.readlines()

# Third line
split = inputContents[2].strip( "\n" ).split( " " )
width = split[0]
height = split[1]

print( width, height )

# Copy header to output
outputFile.write( inputContents[0] )
outputFile.write( inputContents[1] )
outputFile.write( inputContents[2] )

# remove the header, first 3 lines
del inputContents[0]
del inputContents[0]
del inputContents[0]

i = 0

pixels = []
pixel = None

# Load in all the pixels
for line in inputContents:
    
    if ( i == 0 ):
        pixel = []
    
    pixel.append( line.strip( "\n" ) )
        
    if ( i == 2 ):
        pixels.append( pixel )
        pixel = None
        
    i = i + 1
    if ( i == 3 ):
        i = 0

# Write out new image, reverse pixels
for px in pixels:
    choice = random.randrange( 0, 10 )

    # Randomly add noise
    if ( choice == 0 ):
        outputFile.write( str( random.randrange( 0, 255 ) ) ) # R
        outputFile.write( " " ) # Need spaces between numbers
        outputFile.write( str( random.randrange( 0, 255 ) ) ) # G
        outputFile.write( " " ) # Need spaces between numbers
        outputFile.write( str( random.randrange( 0, 255 ) ) ) # B
        outputFile.write( "\n" ) # New line

    # normal colors
    elif ( choice < 1 ):
        outputFile.write( px[0] + " " + px[1] + " " + px[2] + "\n" )

    # flip colors
    elif ( choice < 3 ):
        outputFile.write( px[2] + " " + px[1] + " " + px[0] + "\n" )

    # rand red
    elif ( choice < 6 ):
        outputFile.write( str( random.randrange( 0, 255 ) ) + " " + px[1] + " " + px[2] + "\n" )

    # rand green
    elif ( choice < 8 ):
        outputFile.write( px[0] + " " + str( random.randrange( 0, 255 ) ) + " " + px[2] + "\n" )

    else:
        outputFile.write( px[0] + " " + px[1] + " " + str( random.randrange( 0, 255 ) ) + "\n" )

outputFile.close()
inputFile.close()
