# image-generation

## Image spec

We can build images with a simple file format called ppm

If you open a ppm file with a text editor, you'll see a header,
and a list of Red Green Blue values.

The header looks like:

        P3
        # CREATOR: GIMP PNM Filter Version 1.1
        600 450
        255

* The first value denotes the *type* of PPM file.
* The next two numbers are the dimensions of the image (600 x 450)
* The last header number is the color depth

## Creating an image - Red fill

![Red fill image](example1.png)

Let's say we want to generate an image that is all red.

We need to know the image's width and height, and we need to know what color is red!

All pixels have 3 color values: RED, GREEN, and BLUE. Each color can be between 0 (minimum)
and 255 (maximum). So, ugly bright red is 255 0 0.

We also have to open a file in Python to write to, which is ```myFile``` in the following example.

And we can use two for-loops to fill in the color red for us.

```python
myFile = open( "red.ppm", "w" ) # open red.ppm, to (w)rite to. We access this file via the variable myFile.

# Variables to keep track of width/height
width = 300
height = 250

# Write out header
# \n means new-line
myFile.write( "P3 \n" )
# output image dimensions
myFile.write( str( width ) + " " + str( height ) + " \n" )
# keep it at 255 colors
myFile.write( "255 \n" )

# Draw out colors, each pixel has 3 values: RED GREEN BLUE
# Each color can be between 0 and 255.
# 0 0 0 = black,    255 255 255 = white,
# 255 0 0 = red,    0 255 0 = green,    0 0 255 = blue
for y in range( 0, height ):
    for x in range( 0, width ):
        myFile.write( "255 0 0 \n" )  # RED GREEN BLUE

myFile.close()
```

## Creating an image - Manually drawing pixels

![Random image](example2.png)

*(Blown up to see easier)*

We don't *have* to use a for loop to fill in all the pixels; we can
manually draw each one ourselves. Below, we have a very small image (5x5, or 25 pixels)
and are manually assigning colors to each pixel.


```python
myFile = open( "draw.ppm", "w" ) # open red.ppm, to (w)rite to. We access this file via the variable myFile.

# Variables to keep track of width/height
width = 5
height = 5

# Write out header
# \n means new-line
myFile.write( "P3 \n" )
# output image dimensions
myFile.write( str( width ) + " " + str( height ) + " \n" )
# keep it at 255 colors
myFile.write( "255 \n" )

# Draw pixels out individually!
myFile.write( "255 0 0 \n" )    # pixel 1   Red
myFile.write( "255 0 0 \n" )    # pixel 2   Red
myFile.write( "255 0 0 \n" )    # pixel 3   Red
myFile.write( "0 255 0 \n" )    # pixel 4   Green
myFile.write( "0 255 0 \n" )    # pixel 5   Green
myFile.write( "0 255 0 \n" )    # pixel 6   Green
myFile.write( "0 0 255 \n" )    # pixel 7   Blue
myFile.write( "0 0 255 \n" )    # pixel 8   Blue
myFile.write( "0 0 255 \n" )    # pixel 9   Blue
myFile.write( "255 0 255 \n" )  # pixel 10  Red + Blue = Magenta
myFile.write( "255 255 0 \n" )  # pixel 11  Red + Green = Yellow
myFile.write( "0 255 255 \n" )  # pixel 12  Green + Blue = Cyan
myFile.write( "255 255 255 \n" )    # pixel 13  White
myFile.write( "0 0 0 \n" )          # pixel 14  Black
myFile.write( "100 0 0 \n" )        # pixel 15  Dark red
myFile.write( "0 100 0 \n" )        # pixel 16  Dark green
myFile.write( "0 0 100 \n" )        # pixel 17  Dark blue
myFile.write( "100 0 100 \n" )        # pixel 18  Dark magenta
myFile.write( "100 100 0 \n" )        # pixel 19  Dark yellow
myFile.write( "0 100 100 \n" )        # pixel 20  Dark cyan
myFile.write( "200 200 200 \n" )        # pixel 21  Light gray
myFile.write( "100 100 100 \n" )        # pixel 22  Dark gray
myFile.write( "200 200 200 \n" )        # pixel 23  Light gray
myFile.write( "100 100 100 \n" )        # pixel 24  Dark gray
myFile.write( "255 0 0 \n" )        # pixel 25  Red

myFile.close()
```

## Creating an image - Random colors for every pixel

![Random image](example3.png)

For this one, we are working with code that is similar to our red image
generator, except instead of outputting red for each pixel,
we generate random values for RED, GREEN, and BLUE each time.

```python
import random

myFile = open( "random.ppm", "w" ) # open red.ppm, to (w)rite to. We access this file via the variable myFile.

# Variables to keep track of width/height
width = 300
height = 250

# Write out header
# \n means new-line
myFile.write( "P3 \n" )
# output image dimensions
myFile.write( str( width ) + " " + str( height ) + " \n" )
# keep it at 255 colors
myFile.write( "255 \n" )

# Draw out colors, each pixel has 3 values: RED GREEN BLUE
# Each color can be between 0 and 255.
# 0 0 0 = black,    255 255 255 = white,
# 255 0 0 = red,    0 255 0 = green,    0 0 255 = blue
# Generate random colors
for y in range( 0, height ):
    for x in range( 0, width ):
        myFile.write( str( random.randrange( 0, 255 ) ) ) # R
        myFile.write( " " ) # Need spaces between numbers
        myFile.write( str( random.randrange( 0, 255 ) ) ) # G
        myFile.write( " " ) # Need spaces between numbers
        myFile.write( str( random.randrange( 0, 255 ) ) ) # B
        myFile.write( "\n" ) # New line

myFile.close()
```

