#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>
#include <cstdlib>
using namespace std;

struct RGB
{
    RGB( int r, int g, int b )
    {
        this->r = r;
        this->g = g;
        this->b = b;
    }
    
    int r, g, b;
};


struct Image
{
    string header;
    int width, height;
    vector<RGB> pixels;
};

Image ReadImage( string filename )
{
    Image image;
    
    ifstream input( filename );
    string buffer1, buffer2;

    getline( input, buffer1 );
    getline( input, buffer2 );
    image.header = buffer1 + "\n" + buffer2 + "\n";
    input >> image.width >> image.height;
    
    int r, g, b;
    while ( input >> r >> g >> b )
    {
        RGB rgb( r, g, b );
        image.pixels.push_back( rgb );
    }

    cout << image.pixels.size() << " pixels" << endl;
    
    input.close();

    return image;
}

void WriteImage( Image& image, string filename )
{
    ofstream output( filename );

    output << image.header << endl;
    output << image.width << " " << image.height << endl;
    for ( unsigned int i = 0; i < image.pixels.size(); i++ )
    {
        output
            << image.pixels[i].r << " "
            << image.pixels[i].g << " "
            << image.pixels[i].b
            << endl;
    }
    
    output.close();
}

Image DistortImage( Image image )
{
    for ( unsigned int i = 0; i < image.pixels.size() - 2; i++ )
    {
        if ( image.pixels[i].g > 200 )
        {
            image.pixels[i].g = 50;
        }
        
        if ( image.pixels[i].r > 150 )
        {
            image.pixels[i].r = 255;
        }
        
        if ( image.pixels[i].b < 50 )
        {
            image.pixels[i].b = 255;
        }

        if ( rand() % 5 == 0 )
        {
            image.pixels[i].g = 245;
        }
    }
    return image;
}

Image DistortImage2( Image image )
{
    Image blorp;
    blorp.header = image.header;
    blorp.width = image.width;// image.width / 2;
    blorp.height = image.height; // image.height / 2;
    for ( unsigned int i = 0; i < image.pixels.size(); i += 1 )
    //for ( unsigned int i = image.pixels.size() - 1; i > 0; i-- )
    {
        blorp.pixels.push_back( image.pixels[i] );
        blorp.pixels.push_back( image.pixels[image.pixels.size() - i] );
    }
    return blorp;
}

int main()
{
    srand( time( NULL ) );
    
    vector<string> filenames = {
            "RachRose.ppm"
            /*
            "anim-0.ppm",
            "anim-1.ppm",
            "anim-2.ppm",
            "anim-3.ppm",
            "anim-4.ppm",
            "anim-5.ppm",
            "anim-6.ppm",
            "anim-7.ppm",
            "anim-8.ppm",
            "anim-9.ppm",
            "anim-10.ppm",
            "anim-11.ppm",
            "anim-12.ppm",
            "anim-13.ppm",
            * */
         };

    for ( unsigned int i = 0; i < filenames.size(); i++ )
    {
        cout << filenames[i] << endl;
        Image img = ReadImage( filenames[i] );
        Image distort = DistortImage2( img );
        WriteImage( distort, filenames[i] + "-b.ppm" );
    }

    //system( "convert *-b.ppm asdf.gif" );
    
    return 0;
}
