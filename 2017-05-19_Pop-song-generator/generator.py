import pygame, sys
import random
from pygame.locals import *

pygame.init()
fps = pygame.time.Clock()

window = pygame.display.set_mode( ( 400, 400 ) )
pygame.display.set_caption( "music generator" )

snd_snare1 = pygame.mixer.Sound( "audio/snare1.ogg" )
snd_snare2 = pygame.mixer.Sound( "audio/snare2.ogg" )

snd_toneC = pygame.mixer.Sound( "audio/piano_c.ogg" )
snd_toneD = pygame.mixer.Sound( "audio/piano_d.ogg" )
snd_toneE = pygame.mixer.Sound( "audio/piano_e.ogg" )
snd_toneF = pygame.mixer.Sound( "audio/piano_f.ogg" )
snd_toneG = pygame.mixer.Sound( "audio/piano_g.ogg" )

font = pygame.font.Font( "GlacialIndifference-Regular.otf", 20 )
message = "Hi there"

beat = 0
structure_count = 0
pauseLength = 200
beatsPerMeasure = 8
drums = []

verse = []
bridge = []
chorus = []
reprisal = []
structure = [ "verse", "verse", "verse", "verse", "bridge", "bridge", "chorus", "chorus", "bridge", "bridge" "chorus", "chorus", "verse", "verse", "verse", "verse", "reprisal", "reprisal" ]

for i in range( 0, beatsPerMeasure ): drums.append( 0 )
for i in range( 0, beatsPerMeasure ): verse.append( 0 )
for i in range( 0, beatsPerMeasure ): bridge.append( 0 )
for i in range( 0, beatsPerMeasure ): chorus.append( 0 )
for i in range( 0, beatsPerMeasure ): reprisal.append( 0 )

def GenerateDrumPattern():
    rando = random.randint( 0, 2 )

    if ( rando == 0 ):
        drums[0] = 2
        drums[1] = 1
        drums[3] = 2
        drums[4] = 2
        drums[5] = 1

    if ( rando == 1 ):
        drums[0] = 2
        drums[1] = 2
        drums[2] = 1
        drums[5] = 2
        drums[6] = 2

    if ( rando == 2 ):
        drums[0] = 2
        drums[1] = 1
        drums[3] = 2
        drums[4] = 1
        drums[6] = 2
        drums[7] = 1

    
    
    #for i in range( 0, beatsPerMeasure ):
    #    if ( i == 0 ):
    #        drums[i] = 1
    #    else:
    #        drums[i] = random.randint( 0, 1 )

def GenerateAccompany():
    # C, D, E, F, or G

    # VERSE
    rando = random.randint( 0, 4 )

    if ( rando == 0 ):
        verse[0] = random.randint( 0, 5 )
        verse[3] = random.randint( 0, 5 )
        verse[5] = random.randint( 0, 5 )
        verse[6] = random.randint( 0, 5 )
        
    if ( rando == 1 ):
        verse[0] = random.randint( 0, 5 )
        verse[1] = random.randint( 0, 5 )
        verse[3] = random.randint( 0, 5 )
        verse[4] = random.randint( 0, 5 )
        
    if ( rando == 2 ):
        verse[0] = random.randint( 0, 5 )
        verse[1] = random.randint( 0, 5 )
        verse[2] = random.randint( 0, 5 )
        verse[5] = random.randint( 0, 5 )
        verse[6] = random.randint( 0, 5 )
        verse[7] = random.randint( 0, 5 )
        
    if ( rando == 3 ):
        verse[0] = random.randint( 0, 5 )
        verse[1] = random.randint( 0, 5 )
        verse[2] = random.randint( 0, 5 )
        verse[4] = random.randint( 0, 5 )
        verse[6] = random.randint( 0, 5 )
        
    if ( rando == 4 ):
        verse[0] = random.randint( 0, 5 )
        verse[1] = random.randint( 0, 5 )
        verse[3] = random.randint( 0, 5 )
        verse[5] = random.randint( 0, 5 )
        verse[7] = random.randint( 0, 5 )
        
    # BRIDGE
    rando = random.randint( 0, 2 )

    if ( rando == 0 ):
        bridge[0] = random.randint( 0, 5 )
        bridge[1] = random.randint( 0, 5 )
        bridge[2] = random.randint( 0, 5 )
        bridge[3] = random.randint( 0, 5 )
        
    if ( rando == 1 ):
        bridge[0] = random.randint( 0, 5 )
        bridge[1] = random.randint( 0, 5 )
        bridge[3] = random.randint( 0, 5 )
        bridge[5] = random.randint( 0, 5 )
        bridge[7] = random.randint( 0, 5 )
        
    if ( rando == 2 ):
        bridge[0] = random.randint( 0, 5 )
        bridge[2] = random.randint( 0, 5 )
        bridge[3] = random.randint( 0, 5 )
        bridge[6] = random.randint( 0, 5 )
        bridge[7] = random.randint( 0, 5 )

    # CHORUS    
    rando = random.randint( 0, 3 )

    if ( rando == 0 ):
        chorus[0] = random.randint( 0, 5 )
        chorus[1] = random.randint( 0, 5 )
        chorus[4] = random.randint( 0, 5 )
        chorus[5] = random.randint( 0, 5 )
        chorus[6] = random.randint( 0, 5 )
        
    if ( rando == 1 ):
        chorus[0] = random.randint( 0, 5 )
        chorus[2] = random.randint( 0, 5 )
        chorus[3] = random.randint( 0, 5 )
        chorus[6] = random.randint( 0, 5 )
        chorus[7] = random.randint( 0, 5 )
        
    if ( rando == 2 ):
        chorus[0] = random.randint( 0, 5 )
        chorus[1] = random.randint( 0, 5 )
        chorus[2] = random.randint( 0, 5 )
        chorus[4] = random.randint( 0, 5 )
        chorus[5] = random.randint( 0, 5 )
        chorus[6] = random.randint( 0, 5 )
        
    if ( rando == 3 ):
        chorus[0] = random.randint( 0, 5 )
        chorus[3] = random.randint( 0, 5 )
        chorus[5] = random.randint( 0, 5 )

    for i in range( 0, beatsPerMeasure ):
        reprisal[i] = bridge[i]

        # Change this tone?
        yesno = random.randint( 0, 3 )
        if ( yesno == 0 ):
            reprisal[i] = random.randint( 0, 5 )
        

# This can be cleaned up
def PlayInstruments():
    global beat
    global structure_count
    global message

    message = structure[ structure_count ]

    # Drums
    if ( drums[ beat ] == 2 ):          snd_snare1.play()
    elif ( drums[ beat ] == 1 ):        snd_snare2.play()

    # Tones
    if ( structure[ structure_count ] == "verse" ):
        if ( verse[ beat ] == 1 ):      snd_toneC.play()
        elif ( verse[ beat ] == 2 ):    snd_toneD.play()
        elif ( verse[ beat ] == 3 ):    snd_toneE.play()
        elif ( verse[ beat ] == 4 ):    snd_toneF.play()
        elif ( verse[ beat ] == 5 ):    snd_toneG.play()
        
    if ( structure[ structure_count ] == "bridge" ):
        if ( bridge[ beat ] == 1 ):     snd_toneC.play()
        elif ( bridge[ beat ] == 2 ):   snd_toneD.play()
        elif ( bridge[ beat ] == 3 ):   snd_toneE.play()
        elif ( bridge[ beat ] == 4 ):   snd_toneF.play()
        elif ( bridge[ beat ] == 5 ):   snd_toneG.play()
        
    if ( structure[ structure_count ] == "chorus" ):
        if ( chorus[ beat ] == 1 ):     snd_toneC.play()
        elif ( chorus[ beat ] == 2 ):   snd_toneD.play()
        elif ( chorus[ beat ] == 3 ):   snd_toneE.play()
        elif ( chorus[ beat ] == 4 ):   snd_toneF.play()
        elif ( chorus[ beat ] == 5 ):   snd_toneG.play()
        
    if ( structure[ structure_count ] == "reprisal" ):
        if ( verse[ beat ] == 1 ):      snd_toneC.play()
        elif ( reprisal[ beat ] == 2 ): snd_toneD.play()
        elif ( reprisal[ beat ] == 3 ): snd_toneE.play()
        elif ( reprisal[ beat ] == 4 ): snd_toneF.play()
        elif ( reprisal[ beat ] == 5 ): snd_toneG.play()
        



    beat = beat + 1
    if ( beat == beatsPerMeasure ):
        beat = 0
        structure_count = structure_count + 1

    if ( structure_count == len( structure ) ):
        # song is over
        structure_count = 0
        pygame.time.delay( 1000 )

# Generate song
GenerateDrumPattern()
GenerateAccompany()

print( "Drums: ", drums )
print( "Verse: ", verse )
print( "Bridge: ", bridge )
print( "Chorus: ", chorus )
print( "Reprisal: ", reprisal )

PLAY = USEREVENT + 1
pygame.time.set_timer( PLAY, pauseLength )

# Play song
while True:
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()

        if ( event.type == PLAY ):
            PlayInstruments()

        # Text
        messageObj = font.render( message, False, pygame.Color( 0, 0, 0 ) )
        messageRect = messageObj.get_rect()
        messageRect.topleft = ( 100, 100 )
        window.blit( messageObj, messageRect )

        pygame.display.set_caption( message )

        window.fill( pygame.Color( 255, 255, 255 ) )
        pygame.display.update()

    
    fps.tick( 30 )
