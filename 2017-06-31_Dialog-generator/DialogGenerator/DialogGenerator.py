import random
import os

class DialogGenerator:
    def __init__( self ):
        self.wordLists = {}
        self.wordLists[ "nouns" ] = []
        self.wordLists[ "adjectives" ] = []
        self.wordLists[ "transitive_verbs" ] = []
        
        self.LoadLists()

    def LoadLists( self ):
        print( "Load dialog lists" )
        rootFolder = os.path.join( "lists", os.path.dirname( os.path.realpath( __file__ ) ) )
        contents = list()

        for root, directories, files in os.walk( rootFolder, topdown = False ):
            for name in files:
                contents.append( os.path.join( root, name ) )

        for item in contents:
            if ( "txt" not in item ):
                continue

            wordType = "nouns"
            if ( "noun" in item ):
                wordType = "nouns"
                
            elif ( "adjective" in item ):
                wordType = "adjectives"
                
            elif ( "transitive_verbs" in item ):
                wordType = "transitive_verbs"

            with open( os.path.join( rootFolder, item ), "r" ) as infile:
                for line in infile:
                    trimmed = line.strip( "\n" )
                    self.wordLists[ wordType ].append( trimmed )

        print( "Total words: \n\t" + 
            str( len( self.wordLists["nouns"] ) ) + " nouns, " +
            str( len( self.wordLists["adjectives"] ) ) + " adjectives, " +
            str( len( self.wordLists["transitive_verbs"] ) ) + " transitive verbs" )


    def GetRandomNoun( self ):
        randIndex = random.randint( 0, len( self.wordLists[ "nouns" ] ) - 1 )
        return self.wordLists[ "nouns" ][ randIndex ]


    def GetRandomAdjective( self ):
        randIndex = random.randint( 0, len( self.wordLists[ "adjectives" ] ) - 1 )
        return self.wordLists[ "adjectives" ][ randIndex ]


    def GetRandomTransitiveVerb( self ):
        randIndex = random.randint( 0, len( self.wordLists[ "transitive_verbs" ] ) - 1 )
        return self.wordLists[ "transitive_verbs" ][ randIndex ]

    def GetDialog( self ):
        text1 = ""
        text2 = ""

        text1, topics = self.GetStatement()
        text2 = self.GetResponse( topics )

        print( "Dialog: \n\t (1) " + text1 + ", (2) " + text2 )

        return text1, text2


    def GetStatement( self ):
        randType = random.randint( 0, 20 )

        dialog = ""
        topics = {}
        topics["nouns"] = []
        topics["verbs"] = []
        topics["adjectives"] = []

        if ( randType == 0 ):
            dialog = "I am " + self.GetRandomAdjective()
            topics["nouns"].append( "You" )
            
        elif ( randType == 1 ):
            noun = self.GetRandomNoun()
            dialog = "I don't like " + noun + "s"
            topics["nouns"].append( "You" )
            topics["nouns"].append( noun )
            
        elif ( randType == 2 ):
            adjective = self.GetRandomAdjective()
            dialog = "You are " + adjective
            topics["nouns"].append( "I" )
            topics["adjectives"].append( adjective )
            
        elif ( randType == 3 ):
            noun = self.GetRandomNoun()
            adjective = self.GetRandomAdjective()
            dialog = noun + "s are " + adjective
            topics["nouns"].append( noun )
            topics["adjectives"].append( adjective )
            
        elif ( randType == 4 ):
            verb = self.GetRandomTransitiveVerb()
            dialog = "You " + verb + "ed me."
            topics["nouns"].append( "I" )
            topics["nouns"].append( "You" )
            topics["verbs"].append( verb )

        elif ( randType == 5 ):
            verb = self.GetRandomTransitiveVerb()
            noun = self.GetRandomNoun()
            dialog = "I " + verb + " " + noun + "s."
            topics["nouns"].append( "You" )
            topics["nouns"].append( noun )
            topics["verbs"].append( verb )

        elif ( randType == 6 ):
            verb = self.GetRandomTransitiveVerb()
            dialog = "I want to " + verb
            topics["verbs"].append( verb )

        elif ( randType == 7 ):
            verb = self.GetRandomTransitiveVerb()
            dialog = "I am good at " + verb + "ing"
            topics["verbs"].append( verb )
            
        elif ( randType == 8 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            verb2 = self.GetRandomTransitiveVerb()
            dialog = "When " + noun + "s " + verb + ", I " + verb2
            topics["nouns"].append( noun )
            topics["verbs"].append( verb )
            topics["verbs"].append( verb2 )
            
        elif ( randType == 9 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            dialog = "When I'm alone I " + verb + " " + noun + "s"
            topics["nouns"].append( noun )
            topics["verbs"].append( verb )
            
        elif ( randType == 10 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "Why are " + noun + "s so " + adjective + "?"
            topics["nouns"].append( noun )
            topics["verbs"].append( verb )
            topics["adjectives"].append( adjective )
            
        elif ( randType == 11 ):
            noun = self.GetRandomNoun()
            noun2 = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "My mom always said, \"" + noun + "s " + verb + " " + noun2 + "s\""
            topics["nouns"].append( noun )
            topics["nouns"].append( noun2 )
            topics["nouns"].append( "mom" )
            topics["verbs"].append( verb )
            
        elif ( randType == 12 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "When I think about " + noun + "s, I feel like " + verb + "ing"
            topics["nouns"].append( noun )
            topics["adjectives"].append( adjective )
            
        elif ( randType == 13 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "If " + noun + "s are " + adjective + "... why do they " + verb + "?"
            topics["nouns"].append( noun )
            topics["verbs"].append( verb )
            topics["adjectives"].append( adjective )
            
        elif ( randType == 14 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "I want to live on a " + noun + " planet."
            topics["nouns"].append( noun )
            
        elif ( randType == 15 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "There should be more video games about " + noun + "s."
            topics["nouns"].append( noun )
            topics["nouns"].append( "video game" )
            
        elif ( randType == 16 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "What do you think about " + verb + "ing?"
            topics["verbs"].append( verb )
            
        elif ( randType == 17 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "My favorite " + noun + " always " + verb + "ed"
            topics["verbs"].append( verb )
            
        elif ( randType == 18 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "Both my parents are " + adjective + " because they " + verb + " each other"
            topics["verbs"].append( verb )
            topics["nouns"].append( "parent" )
            
        elif ( randType == 19 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "When I'm scared I " + verb + " nearby " + noun + "s"
            topics["verbs"].append( verb )
            
        elif ( randType == 20 ):
            noun = self.GetRandomNoun()
            verb = self.GetRandomTransitiveVerb()
            adjective = self.GetRandomAdjective()
            dialog = "Stop staring at that " + noun
            topics["nouns"].append( noun )

        return dialog, topics



    def GetResponse( self, topics ):
        dialog = ""

        randomNoun = ""
        randomVerb = ""
        randomAdjective = ""

        if ( len( topics["nouns"] ) > 0 ):
            randomNoun = topics["nouns"][ random.randint( 0, len( topics["nouns"] ) - 1 ) ]
            
        if ( len( topics["verbs"] ) > 0 ):
            randomVerb = topics["verbs"][ random.randint( 0, len( topics["verbs"] ) - 1 ) ]
            
        if ( len( topics["adjectives"] ) > 0 ):
            randomAdjective = topics["adjectives"][ random.randint( 0, len( topics["adjectives"] ) - 1 ) ]
            
        
        randType = random.randint( 0, 3 )

        if ( randType == 0 ):

            # why VERB when you could VERB
            if ( randomVerb != "" ):
                dialog = "Why " + randomVerb + " when you could " + self.GetRandomTransitiveVerb()
                
            # NOUN is something
            elif ( randomNoun != "" ):
                dialog = dialog + randomNoun

                if ( randomNoun == "I" ):
                    dialog = dialog + " am "
                
                elif ( randomNoun == "You" ):
                    dialog = dialog + " are "

                else:
                    dialog = dialog + "s are "
                
                dialog = dialog + self.GetRandomAdjective()

            # ADJECTIVE is ADJECTIVE
            elif ( randomAdjective != "" ):
                dialog = dialog + randomAdjective + " is " + self.GetRandomAdjective()

                
            
        elif ( randType == 1 ):
            # I am told I am ADJECTIVE by NOUN
            if ( randomAdjective != "" ):
                dialog = "Sometimes " + self.GetRandomNoun() + "s tell me that I am " + randomAdjective



            # I'll VERB you!
            elif ( randomVerb != "" ):
                dialog = "I'll " + randomVerb.upper() + " YOU!"
                
            # NOUN is doing something to something else
            elif ( randomNoun != "" ):
                dialog = randomNoun
                                 
                if ( randomNoun == "I" ):
                    dialog = dialog + " am "
                
                elif ( randomNoun == "You" ):
                    dialog = dialog + " are "

                else:
                    dialog = dialog + "s are "

                dialog = dialog + self.GetRandomTransitiveVerb() + "ing "
                dialog = dialog + self.GetRandomNoun()





            
        elif ( randType == 2 ):
            # I VERB NOUNs soemtimes
            if ( randomVerb != "" ):
                dialog = "I " + randomVerb + " " + self.GetRandomNoun() + " sometimes"
                
            # But NOUN is ADJECTIVE
            elif ( randomNoun != "" ):                    
                dialog = "But " + randomNoun
                
                if ( randomNoun == "I" ):
                    dialog = dialog + " am "
                
                elif ( randomNoun == "You" ):
                    dialog = dialog + " are "

                else:
                    dialog = dialog + "s are "
                    
                dialog = dialog + self.GetRandomAdjective()


                
            # I think you're ADJECTIVE
            elif ( randomAdjective != "" ):
                dialog = "I think you're " + randomAdjective





            
        elif ( randType == 3 ):

            # I can't VERB without VERBing
            if ( randomVerb != "" ):
                dialog = "I can't " + randomVerb + " without " + self.GetRandomTransitiveVerb() + "ing " + self.GetRandomNoun() + "s"
                
            # NOUN is also ADJECTIVE
            elif ( randomAdjective != "" ):
                dialog = self.GetRandomNoun() + " is also " + randomAdjective



                

            # And NOUN is ADJECTIVE
            elif ( randomNoun != "" ):
                dialog = "And " + randomNoun
                
                if ( randomNoun == "I" ):
                    dialog = dialog + " am "
                
                elif ( randomNoun == "You" ):
                    dialog = dialog + " are "

                else:
                    dialog = dialog + "s are "
                    
                dialog = dialog + self.GetRandomAdjective()
                





        return dialog




