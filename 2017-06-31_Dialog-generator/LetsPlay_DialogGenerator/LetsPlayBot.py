import random
import os
import pyttsx

ttsEngine = pyttsx.init()

thing = 0

def onStart(name):
   #print 'starting', name
   thing = thing + 1
   
def onWord(name, location, length):
   #print 'word', name, location, length
   thing = thing + 1
   
def onEnd(name, completed):
   thing = thing + 1
   #print 'finishing', name, completed
   
class LetsPlayBot:
    def __init__( self ):
        self.wordLists = {}
        self.name = ""
        self.gameName = ""
        self.voiceName = ""

    def Setup( self, name, voice ):
        self.name = name
        self.voiceName = voice

    def SetGame( self, gameName ):
        self.gameName = gameName

    def GetDialog( self, event, additional=None ):
        
        if ( event == "Introduction 1" ):
            return "Welcome to Robot Let's Plays. I'm " + self.name + "."
            
        elif ( event == "Introduction 2" ):
            return "And I'm " + self.name + "."

        elif ( event == "Introduction 3" ):
            return "And today we're playing " + self.gameName + "."

        elif ( event == "I collect goal" ):
            return event
            
        elif ( event == "Enemy collect goal" ):
            return event
            
        elif ( event == "Both near goal" ):
            return event
            
        elif ( event == "Receive taunt" ):
            return event
            
        elif ( event == "I need one point to win" ):
            return event
            
        elif ( event == "I am ahead by three points" ):
            return event
            
        elif ( event == "Enemy is ahead by three points" ):
            return event
            
        elif ( event == "I win" ):
            return event
            
        elif ( event == "Enemy win" ):
            return event

        else:
            return "Statement"

    def Say( self, event ):
        global ttsEngine
        dialog = self.GetDialog( event )
        print( self.name + ": " + dialog )
        
        ttsEngine.setProperty( "voice", self.voiceName )
        ttsEngine.say( dialog )


# main

bot1 = LetsPlayBot()
bot1.Setup( "Nede", "english-us+f1" )
bot1.SetGame( "Pickin' Sticks" )

bot2 = LetsPlayBot()
bot2.Setup( "Shin", "english-us" )
bot2.SetGame( "Pickin' Sticks" )

bot1.Say( "Introduction 1" )
bot2.Say( "Introduction 2" )
bot1.Say( "Introduction 3" )
ttsEngine.startLoop()
