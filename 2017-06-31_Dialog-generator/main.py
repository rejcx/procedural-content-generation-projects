from DialogGenerator import DialogGenerator
import pyttsx

bot1 = "Ekkie"
bot2 = "Dottie"

thing = 0

def onStart(name):
   #print 'starting', name
   thing = thing + 1
   
def onWord(name, location, length):
   #print 'word', name, location, length
   thing = thing + 1
   
def onEnd(name, completed):
   thing = thing + 1
   #print 'finishing', name, completed
      

dialog = DialogGenerator()

ttsEngine = pyttsx.init()
ttsVoices = ttsEngine.getProperty( "voices" )

ttsEngine.connect('started-utterance', onStart)
ttsEngine.connect('started-word', onWord)
ttsEngine.connect('finished-utterance', onEnd)

dialog = dialog.GetDialog()

print( bot1 + ": " + dialog[0] )
print( bot2 + ": " + dialog[1] )

#for voice in ttsVoices:
    #if ( voice.gender == "female" ):
        #ttsEngine.setProperty( "voice", voice.id )
    #print( voice.id )
    #ttsEngine.setProperty( 'voice', voice.id )
    #ttsEngine.say('The quick brown fox jumped over the lazy dog.')




# Bot 1
ttsEngine.setProperty( "voice", "english-us+f1" )
ttsEngine.say( dialog[0] )
#ttsEngine.runAndWait()

# Bot 2

ttsEngine.setProperty( "voice", "english-us" )
ttsEngine.say( dialog[1] )
#ttsEngine.runAndWait()
ttsEngine.startLoop()
