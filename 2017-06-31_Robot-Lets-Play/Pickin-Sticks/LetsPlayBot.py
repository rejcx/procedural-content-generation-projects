import random
import os
import pyttsx

class WordMaster:
    def __init__( self ):
        self.wordLists = {}
        self.wordLists[ "nouns" ] = []
        self.wordLists[ "good_adjectives" ] = []
        self.wordLists[ "bad_adjectives" ] = []
        self.wordLists[ "transitive_verbs" ] = []
        self.wordLists[ "verb_nouns" ] = []

        self.LoadLists()

    def LoadLists( self ):
        print( "Load word lists" )
        
        rootFolder = os.path.join( "word-lists", os.path.dirname( os.path.realpath( __file__ ) ) )
        contents = list()
        
        for root, directories, files in os.walk( rootFolder, topdown = False ):
            for name in files:
                contents.append( os.path.join( root, name ) )

        for item in contents:
            if ( "txt" not in item ):
                continue

            wordType = "nouns"
            if ( "verb_nouns" in item ):
                wordType = "verb_nouns"
                
            elif ( "noun" in item ):
                wordType = "nouns"
                
            elif ( "bad_adjectives" in item ):
                wordType = "bad_adjectives"
                
            elif ( "good_adjectives" in item ):
                wordType = "good_adjectives"
                
            elif ( "transitive_verbs" in item ):
                wordType = "transitive_verbs"
                

            with open( os.path.join( rootFolder, item ), "r" ) as infile:
                for line in infile:
                    trimmed = line.strip( "\n" )
                    self.wordLists[ wordType ].append( trimmed )

        print( "Total words:" )
        print( "\t" + str( len( self.wordLists["nouns"] ) ) + " nouns" )
        print( "\t" + str( len( self.wordLists["good_adjectives"] ) ) + " good_adjectives" )
        print( "\t" + str( len( self.wordLists["bad_adjectives"] ) ) + " bad_adjectives" )
        print( "\t" + str( len( self.wordLists["transitive_verbs"] ) ) + " transitive_verbs" )
        print( "\t" + str( len( self.wordLists["verb_nouns"] ) ) + " verb_nouns" )


    def GetRandomWord( self, wordType ):
        if ( wordType not in self.wordLists or len( self.wordLists[ wordType ] ) == 0 ):
            return "unknown"
            
        listItem = self.wordLists[ wordType ]
        randIndex = random.randint( 0, len( listItem ) - 1 )
        return listItem[ randIndex ]
        

wordMaster = WordMaster()
                
class LetsPlayBot:
    def __init__( self ):
        self.wordLists = {}
        self.name = ""
        self.gameName = ""
        self.voiceName = ""

    def Setup( self, name, voice ):
        self.name = name
        self.voiceName = voice

    def SetGame( self, gameName ):
        self.gameName = gameName


    def GenerateFailureComment( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 5 )
        
        topics = { "bad_adjective" : "", "good_adjective" : "", "verb" : "", "noun" : "" }

        if ( choice == 0 ):
            dialog = "Next time I'll get it!"

        if ( choice == 1 ):
            topics[ "good_adjective" ] = wordMaster.GetRandomWord( "good_adjectives" )
            dialog = "Don't be so " + topics[ "good_adjective" ] + "!"

        if ( choice == 2 ):
            dialog = "Dang!"

        if ( choice == 3 ):
            dialog = "Slow down!"

        if ( choice == 4 ):
            dialog = "This isn't fair."

        if ( choice == 5 ):
            dialog = "Obviously the sticks generate closer to you!"

        return dialog

    def GenerateSuccessComment( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 5 )
        
        topics = { "bad_adjective" : "", "good_adjective" : "", "verb" : "", "noun" : "" }

        if ( choice == 0 ):
            topics[ "good_adjective" ] = wordMaster.GetRandomWord( "good_adjectives" )
            dialog = "Winning makes me feel " + topics[ "good_adjective" ]

        if ( choice == 1 ):
            topics[ "verb" ] = wordMaster.GetRandomWord( "verb_nouns" )
            dialog = "Not only am I good at " + topics[ "verb" ] + " but I'm good at pickin' sticks, too."

        if ( choice == 2 ):
            topics[ "good_adjective" ] = wordMaster.GetRandomWord( "good_adjectives" )
            dialog = "I am " + topics[ "good_adjective" ]

        if ( choice == 3 ):
            topics[ "noun" ] = wordMaster.GetRandomWord( "verb_nouns" )
            dialog = "I love this almost as much as I love " + topics[ "noun" ]

        if ( choice == 4 ):
            topics[ "verb" ] = wordMaster.GetRandomWord( "transitive_verbs" )
            dialog = "I will " + topics[ "verb" ] + " you!"

        if ( choice == 5 ):
            topics[ "bad_adjective" ] = wordMaster.GetRandomWord( "bad_adjectives" )
            dialog = "Don't be so " + topics[ "bad_adjective" ] + "!"

        return dialog

    def GenerateInsult( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 5 )

        topics = { "bad_adjective" : "", "verb" : "", "noun" : "" }

        if ( choice == 0 ):
            topics[ "bad_adjective" ] = wordMaster.GetRandomWord( "bad_adjectives" )
            dialog = "You're so " + topics[ "bad_adjective" ]

        elif ( choice == 1 ):
            topics[ "noun" ] = "tree"
            dialog = "I've seen " + topics[ "noun" ] + " that are better than you."

        elif ( choice == 2 ):
            topics[ "noun" ] = "tree"
            topics[ "verb" ] = "climb"
            dialog = "If I were you, I'd " + topics[ "verb" ] + " a " + topics[ "noun" ]

        elif ( choice == 3 ):
            topics[ "bad_adjective" ] = wordMaster.GetRandomWord( "bad_adjectives" )
            dialog = "Why are you " + topics[ "bad_adjective" ] + "?"

        elif ( choice == 4 ):
            topics[ "bad_adjective" ] = wordMaster.GetRandomWord( "bad_adjectives" )
            dialog = "Where did you train? At " + topics[ "bad_adjective" ] + " school?"

        elif ( choice == 5 ):
            topics[ "verb" ] = wordMaster.GetRandomWord( "transitive_verbs" )
            dialog = "Come on, the stick won't " + topics[ "verb" ] + " you. What are you afraid of?"

        return dialog, topics


    def GenerateComeback( self, topics ):
        dialog = ""
        
        if ( topics[ "bad_adjective" ] != "" ):
            dialog = ""
            
        elif ( topics[ "verb" ] != "" ):
            dialog = ""
            
        elif ( topics[ "noun" ] != "" ):
            dialog = ""

        return dialog


    def GenerateBothNearComment( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 3 )

        if ( choice == 0 ):
            dialog = "The stick is mine!"
            
        elif ( choice == 1 ):
            dialog = "Look! A three-headed monkey!"
            
        elif ( choice == 2 ):
            dialog = "Stop!"
            
        elif ( choice == 3 ):
            dialog = "Get away from there!"

        return dialog


    def GenerateWinComment( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 2 )
        
        topics = { "bad_adjective" : "", "good_adjective" : "", "verb" : "", "noun" : "" }

        if ( choice == 0 ):
            topics[ "good_adjective" ] = wordMaster.GetRandomWord( "good_adjectives" )
            dialog = "I won because I am " + topics[ "good_adjective" ]
            
        elif ( choice == 1 ):
            dialog = "I win!"
            
        elif ( choice == 2 ):
            topics[ "bad_adjective" ] = wordMaster.GetRandomWord( "bad_adjectives" )
            topics[ "good_adjective" ] = wordMaster.GetRandomWord( "good_adjectives" )
            dialog = "I'm " + topics[ "good_adjective" ] + " you're " + topics[ "bad_adjective" ] + "!"

        return dialog


    def GenerateOnePointLeftComment( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 2 )
        
        topics = { "bad_adjective" : "", "good_adjective" : "", "verb" : "", "noun" : "" }

        if ( choice == 0 ):
            dialog = "One point left!"
            
        elif ( choice == 1 ):
            dialog = "This is the moment of truth!"
            
        elif ( choice == 2 ):
            dialog = "Who will win?!"

        return dialog


    def GenerateAheadByThreePointsComment( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 2 )
        
        topics = { "bad_adjective" : "", "good_adjective" : "", "verb" : "", "noun" : "" }

        if ( choice == 0 ):
            topics[ "good_adjective" ] = wordMaster.GetRandomWord( "good_adjectives" )
            dialog = "I am so incredibly " + topics[ "good_adjective" ]
            
        elif ( choice == 1 ):
            dialog = "You will never catch up to me!"
            
        elif ( choice == 2 ):
            dialog = "I am good!"

        return dialog


    def GeneratePowerupComment( self ):
        global wordMaster
        
        dialog = ""
        choice = random.randint( 0, 2 )
        
        topics = { "bad_adjective" : "", "good_adjective" : "", "verb" : "", "noun" : "" }

        if ( choice == 0 ):
            topics[ "good_adjective" ] = wordMaster.GetRandomWord( "good_adjectives" )
            dialog = "This will make me more " + topics[ "good_adjective" ] + "!"
            
        elif ( choice == 1 ):
            dialog = "This is just what I needed!"
            
        elif ( choice == 2 ):
            topics[ "verb" ] = wordMaster.GetRandomWord( "transitive_verbs" )
            dialog = "Now I will " + topics[ "verb" ] + " you!"

        return dialog
        
        

    def GetDialog( self, event, additional=None ):
        
        if ( event == "Introduction 1" ):
            return "Welcome to Robot Let's Plays. I'm " + self.name + "."
            
        elif ( event == "Introduction 2" ):
            return "And I'm " + self.name + "."

        elif ( event == "Introduction 3" ):
            return "And today we're playing " + self.gameName + "."


        if ( event == "End 1" ):
            return "That's all we have time for today."
            
        elif ( event == "End 2" ):
            return "Thanks for watching!"
            
        elif ( event == "End 3" ):
            return "See you next time!"



        elif ( event == "I collect goal" ):
            #return event
            return self.GenerateSuccessComment()
            
        elif ( event == "Enemy collect goal" ):
            return self.GenerateFailureComment()
            
        elif ( event == "Both near goal" ):
            return self.GenerateBothNearComment()

        elif ( event == "Get taunt" ):
            return self.GenerateInsult()
            
        elif ( event == "Receive taunt" ):
            return event
            
        elif ( event == "I need one point to win" ):
            return self.GenerateOnePointLeftComment()
            
        elif ( event == "I am ahead by three points" ):
            return self.GenerateAheadByThreePointsComment()

        elif ( event == "Got powerup" ):
            return self.GeneratePowerupComment()
            
        elif ( event == "Enemy is ahead by three points" ):
            return event
            
        elif ( event == "I win" ):
            return self.GenerateWinComment()
            
        elif ( event == "Enemy wins" ):
            return event

        else:
            return "Statement"



