import pygame, sys, math, random
from pygame.locals import *
import os

import LetsPlayBot

from gtts import gTTS

os.system( "mkdir talk" )

audioCounter = 0

def Say( bot, dialog ):
    global audioCounter   
    print( bot.name + ": " + dialog )

    voice1 = "hi"#"en-us"
    voice2 = "en-uk"

    voice = voice2
    if ( bot.name == "Nede" ):
        voice = voice1

    filename = "talk/temp_" + str( audioCounter )
    audioCounter = audioCounter + 1
    filemp3 = filename + ".mp3"
    fileogg = filename + ".ogg"
    
    tts = gTTS( text = dialog, lang = voice, slow = False )
    tts.save( filemp3 )

    # Play it with pygame
    #pygame.mixer.music.load( dialog + ".mp3" )
    #pygame.mixer.music.play()

    # kludgey as fuck
    cmd = "ffmpeg -i " + filemp3 + " " + fileogg
    print( cmd )
    os.system( cmd )
    
    audio = pygame.mixer.Sound( fileogg )
    audio.play()

    if ( bot.name == "Nede" ):
        global fontDialog
        global txtPlayer1Dialog
        global txtPlayer1DialogRect

        txtPlayer1Dialog = fontDialog.render( dialog, False, colorText )
        txtPlayer1DialogRect = txtPlayer1Dialog.get_rect()
        txtPlayer1DialogRect.topleft = ( 10, screenHeight - 40 )

    elif ( bot.name == "Shin" ):
        global fontDialog
        global txtPlayer2Dialog
        global txtPlayer2DialogRect
        
        txtPlayer2Dialog = fontDialog.render( dialog, False, colorText )
        txtPlayer2DialogRect = txtPlayer2Dialog.get_rect()
        txtPlayer2DialogRect.topleft = ( screenWidth - 400, screenHeight - 40 )


class Player:
    def __init__( self ):
        self.x = 0
        self.y = 0
        self.width = 32
        self.height = 48
        self.points = 0
        self.name = "Player"
        self.frame = 0
        self.direction = 0
        self.speed = 5
        self.animateSpeed = 0.3
        self.powerupCountdown = 0

        self.ai_direction = 0
        self.ai_counter = 0
        self.ai_increment = 5

    def Setup( self, playerName, startingX, startingY ):
        self.name = playerName
        self.x = startingX
        self.y = startingY

    def Draw( self, window, image ):
        rect = image.get_rect()
        rect.topleft = ( self.x, self.y )
        window.blit( image, rect, ( math.floor( self.frame ) * self.width, self.direction * self.height, self.width, self.height ) )
        

    def IncreaseSpeed( self ):
        self.speed = 10
        self.powerupCountdown = 100

    def Update( self ):
        if ( self.powerupCountdown > 0 ):
            self.powerupCountdown = self.powerupCountdown - 1

            if ( self.powerupCountdown == 0 ):
                self.speed = 5

    def Move( self, xDir, yDir ):
        self.x = self.x + xDir * self.speed
        self.y = self.y + yDir * self.speed

        if ( xDir < 0 ): # left
            self.direction = 2
            
        elif ( xDir > 0 ): # right
            self.direction = 3
            
        elif ( yDir < 0 ):  # up
            self.direction = 1
            
        elif ( yDir > 0 ):  # down
            self.direction = 0
        
        self.Animate()

    def Animate( self ):
        self.frame = self.frame + self.animateSpeed

        if ( self.frame >= 4 ):
            self.frame = 0

    def AddToScore( self ):
        self.points = self.points + 1
        print( self.name + ": " + str( self.points ) )

    def DecideMovement( self, goal, secondaryGoal ):

        currentGoal = goal

        if ( self.ai_counter == 0 ):
            if ( secondaryGoal.GetDistance( self ) < 200 ):
                # Move toward powerup
                currentGoal = secondaryGoal

            
            
            self.ai_counter = self.ai_increment
            self.ai_direction = random.randint( 1, 2 )

            if ( self.ai_direction == 1 ): # move horizontal

                if ( currentGoal.x < self.x ):
                    self.ai_direction = 0
                    
                elif ( currentGoal.x > self.x ):
                    self.ai_direction = 1
                
            else: # move vertical
                
                if ( currentGoal.y < self.y ):
                    self.ai_direction = 2
                    
                elif ( currentGoal.y > self.y ):
                    self.ai_direction = 3
        else:
            self.ai_counter = self.ai_counter - 1

                
        if ( self.ai_direction == 0 ):
            self.Move( -1, 0 )
            
        elif ( self.ai_direction == 1 ):
            self.Move( 1, 0 )
            
        elif ( self.ai_direction == 2 ):
            self.Move( 0, -1 )
            
        elif ( self.ai_direction == 3 ):
            self.Move( 0, 1 )

class Item:
    def __init__( self, name ):
        self.name = name
        self.x = 0
        self.y = 0
        self.width = 32
        self.height = 32
        self.countdown = 0

    def GeneratePosition( self, maxX, maxY ):
        self.x = random.randint( 0, maxX - self.width )
        self.y = random.randint( 0, maxY - self.height * 3 )
        self.countdown = 100
        
        
    def Draw( self, window, image ):
        rect = image.get_rect()
        rect.topleft = ( self.x, self.y )
        window.blit( image, rect )

    def Colliding( self, playerThing ):
        return (    self.x < playerThing.x + playerThing.width and
                    self.x + self.width > playerThing.x and
                    self.y < playerThing.y + playerThing.height and
                    self.y + self.height > playerThing.y )

    def GetDistance( self, playerThing ):
        return math.sqrt( ( playerThing.x - self.x )**2 + ( playerThing.y - self.y )**2 )

    def Update( self, screenWidth, screenHeight ):
        if ( self.name == "candy" and self.countdown > 0 ):
            self.countdown = self.countdown - 1

            if ( self.countdown == 0 ):
                self.GeneratePosition( screenWidth, screenHeight )
                global soundMoveCandy
                soundMoveCandy.play()


bot1 = LetsPlayBot.LetsPlayBot()
bot1.Setup( "Nede", "english-us+f1" )
bot1.SetGame( "Pickin' Sticks" )

bot2 = LetsPlayBot.LetsPlayBot()
bot2.Setup( "Shin", "english-us" )
bot2.SetGame( "Pickin' Sticks" )

pygame.init()
fps = pygame.time.Clock()

pygame.mixer.pre_init(22050, -16, 2, 2048)
pygame.mixer.init()

screenWidth = 1280
screenHeight = 720

winscore = 10

window = pygame.display.set_mode( ( screenWidth, screenHeight ) )
pygame.display.set_caption( "Pickin' Sticks CCXXXV" )

imgGrass = pygame.image.load( "assets/grass.png" )
imgStick = pygame.image.load( "assets/stick.png" )
imgPlayer1 = pygame.image.load( "assets/Delphine_Moosader.png" )
imgPlayer2 = pygame.image.load( "assets/Ayne_Moosader.png" )
imgCandy = pygame.image.load( "assets/candy.png" )

colorEmptyBg = pygame.Color( 100, 100, 100 )
colorText = pygame.Color( 255, 255, 255 )

SONG_END = pygame.USEREVENT + 1
pygame.mixer.music.load( "assets/ClearDay_Moosader.ogg" )
#pygame.mixer.music.set_volume( 0.05 )
#pygame.mixer.music.play()

soundCollect = pygame.mixer.Sound( "assets/pickup.wav" )
soundPowerup = pygame.mixer.Sound( "assets/powerup.wav" )
soundMoveCandy = pygame.mixer.Sound( "assets/candymove.wav" )


player1 = Player()
player1.Setup( "Nede", 100, 100 )
player1.ai_increment = random.randint( 5, 10 )

player2 = Player()
player2.Setup( "Shin", screenWidth - 32 - 100, 100 )
player2.ai_increment = random.randint( 5, 10 )

stick = Item( "stick" )
stick.GeneratePosition( screenWidth, screenHeight )

candy = Item( "candy" )
candy.GeneratePosition( screenWidth, screenHeight )

font = pygame.font.Font( "assets/PressStart2P.ttf", 30 )
fontSmall = pygame.font.Font( "assets/PressStart2P.ttf", 15 )
fontDialog = pygame.font.Font( "assets/PressStart2P.ttf", 11 )

txtPlayer1Score = font.render( player1.name + ": " + str( player1.points ), False, colorText )
txtPlayer1ScoreRect = txtPlayer1Score.get_rect()
txtPlayer1ScoreRect.topleft = ( 10, screenHeight - 100 )

txtPlayer1Name = fontSmall.render( player1.name, False, colorText )
txtPlayer1NameRect = txtPlayer1Name.get_rect()
txtPlayer1NameRect.topleft = ( player1.x, player1.y - 20 )

txtPlayer2Score = font.render( player2.name + ": " + str( player2.points ), False, colorText )
txtPlayer2ScoreRect = txtPlayer2Score.get_rect()
txtPlayer2ScoreRect.topleft = ( screenWidth - 400, screenHeight - 100 )

txtPlayer2Name = fontSmall.render( player2.name, False, colorText )
txtPlayer2NameRect = txtPlayer2Name.get_rect()
txtPlayer2NameRect.topleft = ( player2.x, player2.y - 20 )

txtPlayer1Dialog = fontDialog.render( "", False, colorText )
txtPlayer1DialogRect = txtPlayer1Dialog.get_rect()
txtPlayer1DialogRect.topleft = ( 10, screenHeight - 40 )

txtPlayer2Dialog = fontDialog.render( "", False, colorText )
txtPlayer2DialogRect = txtPlayer2Dialog.get_rect()
txtPlayer2DialogRect.topleft = ( screenWidth - 400, screenHeight - 40 )

gameover = False

txtWinner = font.render( "Player 2 wins!", False, colorText )
txtWinnerRect = txtWinner.get_rect()
txtWinnerRect.topleft = ( screenWidth/2 - 200, screenHeight/2 - 50 )

introCount = 10

done = False

while done == False:

    # Event update
    player1.Update()
    player2.Update()
    candy.Update( screenWidth, screenHeight )

    for event in pygame.event.get():
        if ( event.type == QUIT ):
            os.system( "rm talk -r" )

            pygame.quit()
            sys.exit()

        if ( event.type == SONG_END ):
            pygame.mixer.music.play()

    keypress = pygame.key.get_pressed()

    
    # Player 1 movement
    if ( keypress[ pygame.K_LEFT ] ):
        player1.Move( -1, 0 )
        
    elif ( keypress[ pygame.K_RIGHT ] ):
        player1.Move( 1, 0 )
        
    elif ( keypress[ pygame.K_UP ] ):
        player1.Move( 0, -1 )
        
    elif ( keypress[ pygame.K_DOWN ] ):
        player1.Move( 0, 1 )

    player1.DecideMovement( stick, candy )

        
    # Player 2 movement
    if ( keypress[ pygame.K_a ] ):
        player2.Move( -1, 0 )
        
    elif ( keypress[ pygame.K_d ] ):
        player2.Move( 1, 0 )
        
    elif ( keypress[ pygame.K_w ] ):
        player2.Move( 0, -1 )
        
    elif ( keypress[ pygame.K_s ] ):
        player2.Move( 0, 1 )

    player2.DecideMovement( stick, candy )

        
        



    if ( keypress[ pygame.K_x ] ):        
        Say( bot1, bot1.GetDialog( "I collect goal" ) )
        Say( bot2, bot2.GetDialog( "Enemy collect goal" ) )
            
        

    # Check collision
    if ( stick.Colliding( player1 ) ):
        player1.AddToScore()
        stick.GeneratePosition( screenWidth, screenHeight )
        txtPlayer1Score = font.render( player1.name + ": " + str( player1.points ), False, colorText )
        soundCollect.play()

        whoSpeaks = random.randint( 1, 2 )
        if ( whoSpeaks == 1 ):
            Say( bot1, bot1.GetDialog( "I collect goal" ) )
        
        else:
            Say( bot2, bot2.GetDialog( "Enemy collect goal" ) )
        
        if ( player1.points - player2.points == 3 ):
            Say( bot1, bot1.GetDialog( "I am ahead by three points" ) )
        

    if ( stick.Colliding( player2 ) ):
        player2.AddToScore()
        stick.GeneratePosition( screenWidth, screenHeight )
        txtPlayer2Score = font.render( player2.name + ": " + str( player2.points ), False, colorText )
        soundCollect.play()
        
        whoSpeaks = random.randint( 1, 2 )
        if ( whoSpeaks == 1 ):
            Say( bot1, bot1.GetDialog( "Enemy collect goal" ) )

        else:
            Say( bot2, bot2.GetDialog( "I collect goal" ) )

        if ( player2.points - player1.points == 3 ):
            Say( bot1, bot1.GetDialog( "I am ahead by three points" ) )


    if ( candy.Colliding( player1 ) ):
        player1.IncreaseSpeed()
        soundPowerup.play()
        Say( bot1, bot1.GetDialog( "Got powerup" ) )
        candy.GeneratePosition( screenWidth, screenHeight )

    elif ( candy.Colliding( player2 ) ):
        player2.IncreaseSpeed()
        soundPowerup.play()
        Say( bot2, bot2.GetDialog( "Got powerup" ) )
        candy.GeneratePosition( screenWidth, screenHeight )


    elif ( stick.GetDistance( player1 ) < 100 and stick.GetDistance( player2 ) < 100 ):
        whoSpeaks = random.randint( 1, 5 )
        if ( whoSpeaks == 1 ):
            Say( bot1, bot1.GetDialog( "Both near goal" ) )

        elif ( whoSpeaks == 2 ):
            Say( bot2, bot2.GetDialog( "Both near goal" ) )

        # Otherwise - don't gotta speak every time!


    if ( player1.points >= winscore and gameover == False ):
        gameover = True
        txtWinner = font.render( player1.name + " wins!", False, colorText )
        
        Say( bot1, bot1.GetDialog( "I win" ) )
        done = True


    if ( player2.points >= winscore and gameover == False ):
        gameover = True
        txtWinner = font.render( player2.name + " wins!", False, colorText )
        
        Say( bot2, bot2.GetDialog( "I win" ) )
        done = True


    
    # Draw
    
    window.fill( colorEmptyBg )

    for y in range( 0, screenHeight / 32 + 1 ):
        for x in range( 0, screenWidth / 32 ):
            rect = imgGrass.get_rect()
            rect.topleft = ( x * 32, y * 32 )
            window.blit( imgGrass, rect )

            
    txtPlayer1NameRect.topleft = ( player1.x, player1.y - 60 )
    txtPlayer2NameRect.topleft = ( player2.x, player2.y - 60 )

    stick.Draw( window, imgStick )
    candy.Draw( window, imgCandy )
    player1.Draw( window, imgPlayer1 )
    player2.Draw( window, imgPlayer2 )

    window.blit( txtPlayer1Score, txtPlayer1ScoreRect )
    window.blit( txtPlayer2Score, txtPlayer2ScoreRect )
    window.blit( txtPlayer1Name, txtPlayer1NameRect )
    window.blit( txtPlayer2Name, txtPlayer2NameRect )
    window.blit( txtPlayer1Dialog, txtPlayer1DialogRect )
    window.blit( txtPlayer2Dialog, txtPlayer2DialogRect )


    if ( gameover == True ):
        window.blit( txtWinner, txtWinnerRect )




        
    pygame.display.update()
    fps.tick( 30 )
    

    if ( introCount > 0 ):

        #if ( introCount == 3 ):
            #Say( bot1, bot1.GetDialog( "Introduction 1" ) )
        #elif ( introCount == 2 ):
            #Say( bot2, bot1.GetDialog( "Introduction 2" ) )
        #elif ( introCount == 1 ):
            #Say( bot1, bot1.GetDialog( "Introduction 3" ) )

        introCount = 0
      
        if ( True ):
            Say( bot1, bot1.GetDialog( "Introduction 1" ) )

            window.fill( colorEmptyBg )
            for y in range( 0, screenHeight / 32 + 1 ):
                for x in range( 0, screenWidth / 32 ):
                    rect = imgGrass.get_rect()
                    rect.topleft = ( x * 32, y * 32 )
                    window.blit( imgGrass, rect )
                    
            player1.Draw( window, imgPlayer1 )
            player2.Draw( window, imgPlayer2 )
            window.blit( txtPlayer1Score, txtPlayer1ScoreRect )
            window.blit( txtPlayer2Score, txtPlayer2ScoreRect )
            window.blit( txtPlayer1Name, txtPlayer1NameRect )
            window.blit( txtPlayer2Name, txtPlayer2NameRect )
            window.blit( txtPlayer1Dialog, txtPlayer1DialogRect )
            window.blit( txtPlayer2Dialog, txtPlayer2DialogRect )
            
            pygame.display.update()

            pygame.time.delay( 3000 )
            
            Say( bot2, bot2.GetDialog( "Introduction 2" ) )
            
            window.fill( colorEmptyBg )
            for y in range( 0, screenHeight / 32 + 1 ):
                for x in range( 0, screenWidth / 32 ):
                    rect = imgGrass.get_rect()
                    rect.topleft = ( x * 32, y * 32 )
                    window.blit( imgGrass, rect )
                    
            player1.Draw( window, imgPlayer1 )
            player2.Draw( window, imgPlayer2 )
            window.blit( txtPlayer1Score, txtPlayer1ScoreRect )
            window.blit( txtPlayer2Score, txtPlayer2ScoreRect )
            window.blit( txtPlayer1Name, txtPlayer1NameRect )
            window.blit( txtPlayer2Name, txtPlayer2NameRect )
            window.blit( txtPlayer1Dialog, txtPlayer1DialogRect )
            window.blit( txtPlayer2Dialog, txtPlayer2DialogRect )
            
            pygame.display.update()
            
            pygame.time.delay( 1000 )
            
            Say( bot1, bot1.GetDialog( "Introduction 3" ) )
            
            window.fill( colorEmptyBg )
            for y in range( 0, screenHeight / 32 + 1 ):
                for x in range( 0, screenWidth / 32 ):
                    rect = imgGrass.get_rect()
                    rect.topleft = ( x * 32, y * 32 )
                    window.blit( imgGrass, rect )
                    
            player1.Draw( window, imgPlayer1 )
            player2.Draw( window, imgPlayer2 )
            window.blit( txtPlayer1Score, txtPlayer1ScoreRect )
            window.blit( txtPlayer2Score, txtPlayer2ScoreRect )
            window.blit( txtPlayer1Name, txtPlayer1NameRect )
            window.blit( txtPlayer2Name, txtPlayer2NameRect )
            window.blit( txtPlayer1Dialog, txtPlayer1DialogRect )
            window.blit( txtPlayer2Dialog, txtPlayer2DialogRect )
            
            pygame.display.update()
            
            pygame.time.delay( 1000 )


# End of game

if ( True ):

    pygame.time.delay( 3000 )

    Say( bot1, bot1.GetDialog( "End 1" ) )

    pygame.time.delay( 2000 )

    Say( bot2, bot2.GetDialog( "End 2" ) )

    pygame.time.delay( 2000 )

    Say( bot1, bot2.GetDialog( "End 3" ) )

    pygame.time.delay( 3000 )


# delete the talk folder
os.system( "rm talk -r" )

pygame.quit()
sys.exit()
