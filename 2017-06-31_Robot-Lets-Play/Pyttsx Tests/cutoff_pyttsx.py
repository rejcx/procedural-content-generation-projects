import pyttsx

# In this example, subsequent audio clips to say are cut short.
# I thought this was maybe a problem with the game loop in pygame,
# but evidently not. runAndWait isn't quite doing its job...

ttsEngine = pyttsx.init()


def onStart(name):
    print 'A. starting', name
    
   
def onWord(name, location, length):
    print '\t B. word', name, location, length
   
def onEnd(name, completed):
    print 'C. finishing', name, completed


ttsEngine.connect('started-utterance', onStart)
ttsEngine.connect('started-word', onWord)
ttsEngine.connect('finished-utterance', onEnd)

def Say( dialog, ttsEngine ):
    print( "Say: " + dialog )
    ttsEngine.say( dialog )
    ttsEngine.runAndWait()

Say( "Hello there 1", ttsEngine )
Say( "Hello there 2", ttsEngine )
Say( "Hello there 3", ttsEngine )
Say( "Hello there 4", ttsEngine )
Say( "Hello there 5", ttsEngine )
Say( "Hello there 6", ttsEngine )


            
