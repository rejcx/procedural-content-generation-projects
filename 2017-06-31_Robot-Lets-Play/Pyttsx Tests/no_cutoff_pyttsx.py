import pyttsx

# In this example, the words aren't cut off if they're all queued up first
# and then runAndWait is called. However, this isn't practical for my game.

ttsEngine = pyttsx.init()


def onStart(name):
    print 'A. starting', name
    
   
def onWord(name, location, length):
    print '\t B. word', name, location, length
   
def onEnd(name, completed):
    print 'C. finishing', name, completed


ttsEngine.connect('started-utterance', onStart)
ttsEngine.connect('started-word', onWord)
ttsEngine.connect('finished-utterance', onEnd)

def Say( dialog, ttsEngine ):
    print( "Say: " + dialog )
    ttsEngine.say( dialog )

def RunAndWait( ttsEngine ):
    ttsEngine.runAndWait()

Say( "Hello there 1", ttsEngine )
Say( "Hello there 2", ttsEngine )
Say( "Hello there 3", ttsEngine )
Say( "Hello there 4", ttsEngine )
Say( "Hello there 5", ttsEngine )
Say( "Hello there 6", ttsEngine )

RunAndWait( ttsEngine )

            
