import pyttsx

# This one skips the even numbered text but not the odd numbered ones

ttsEngine = pyttsx.init()


def onStart(name):
    print 'A. starting', name
    
   
def onWord(name, location, length):
    print '\t B. word', name, location, length
   
def onEnd(name, completed):
    print 'C. finishing', name, completed
    global ttsEngine
    ttsEngine.endLoop()

def onError( name, exception ):
    print( "ERROR" )
    print( name )
    print( exception )

ttsEngine.connect('started-utterance', onStart)
ttsEngine.connect('started-word', onWord)
ttsEngine.connect('finished-utterance', onEnd)
ttsEngine.connect('error', onError)

def Say( dialog, ttsEngine ):
    engineBusy = False #ttsEngine.isBusy()
    print( "Is the engine busy?", engineBusy )
    
    if ( engineBusy == False ):
        print( "Say: " + dialog )
        ttsEngine.say( dialog )
        # RunAndWait( ttsEngine )
        StartLoop( ttsEngine )
        

def RunAndWait( ttsEngine ):
    ttsEngine.runAndWait()

def StartLoop( ttsEngine ):
    ttsEngine.startLoop()

ttsEngine.stop()

Say( "Hello there 1", ttsEngine )
Say( "Hello there 2", ttsEngine )
Say( "Hello there 3", ttsEngine )
Say( "Hello there 4", ttsEngine )
Say( "Hello there 5", ttsEngine )
Say( "Hello there 6", ttsEngine )

#RunAndWait( ttsEngine )

#StartLoop( ttsEngine )
            
