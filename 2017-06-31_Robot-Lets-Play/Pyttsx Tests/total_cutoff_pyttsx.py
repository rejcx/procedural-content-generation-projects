import pyttsx

# This is from the docs:
# https://pyttsx.readthedocs.io/en/latest/engine.html#examples
# but it doesn't work

def externalLoop( ttsEngine ):
    ttsEngine.iterate()

engine = pyttsx.init()
engine.say('The quick brown fox jumped over the lazy dog.', 'fox')
engine.startLoop(False)
# engine.iterate() must be called inside externalLoop()
externalLoop( engine )
engine.endLoop()
