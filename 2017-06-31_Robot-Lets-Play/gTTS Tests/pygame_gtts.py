# This Python file uses the following encoding: utf-8
# https://pypi.python.org/pypi/gTTS

# https://stackoverflow.com/questions/7746263/how-play-mp3-with-pygame#8415875

from gtts import gTTS

import pygame, sys, math, random
from pygame.locals import *

#  pygame.mixer.pre_init(frequency=22050, size=-16, channels=1, buffer=256) 
pygame.mixer.pre_init(22050, -16, 2, 2048)
pygame.mixer.init()




def File( dialog ):
    print( "File " + dialog )
    
    tts = gTTS( text = dialog, lang = "en", slow = True )
    tts.save( dialog + ".mp3" )

def Say( dialog ):
    print( "Say " + dialog )
    
    tts = gTTS( text = dialog, lang = "en", slow = True )
    tts.save( dialog + ".mp3" )

    # Load with pygame
    talk = pygame.mixer.Sound( dialog + ".mp3" )
    talk.play()

def MixerSay( dialog ):
    print( "MixerSay " + dialog )
    
    tts = gTTS( text = dialog, lang = "en", slow = True )
    tts.save( dialog + ".mp3" )

    
    pygame.mixer.music.load( dialog + ".mp3" )

    pygame.mixer.music.play()

pygame.init()
fps = pygame.time.Clock()

screenWidth = 1280
screenHeight = 720

window = pygame.display.set_mode( ( screenWidth, screenHeight ) )
pygame.display.set_caption( "gTTS Test" )

colorEmptyBg = pygame.Color( 100, 100, 100 )

sound1 = pygame.mixer.Sound( "pickup.wav" )
sound2 = pygame.mixer.Sound( "pickup.mp3" )
sound3 = pygame.mixer.Sound( "hello.mp3" )

while True:
    window.fill( colorEmptyBg )

    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()


    keypress = pygame.key.get_pressed()
    
    if ( keypress[ pygame.K_x ] ):
        MixerSay( "cats are cool" )

    if ( keypress[ pygame.K_a ] ):
        sound1.play()

    if ( keypress[ pygame.K_b ] ):
        sound2.play()

    pygame.display.update()
    fps.tick( 30 )
