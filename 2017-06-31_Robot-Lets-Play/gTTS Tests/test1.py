# This Python file uses the following encoding: utf-8
# https://pypi.python.org/pypi/gTTS

from gtts import gTTS

tts = gTTS( text = "Hello how the heck are you?", lang = "en", slow = True )
tts.save( "Hello.mp3" )

# But I don't want to save mp3 files!

tts = gTTS( text = "नमस्ते, क्या हाल है?", lang = "hi", slow = True )
tts.save( "Namaste.mp3" )
