import os
import tweepy, time, sys
import api_info
import random

# http://www.dototot.com/how-to-write-a-twitter-bot-with-python-and-tweepy/

print( "\n GOOD MORNING \n" )
offline = False

api = None

class Vocabulary:
	def __init__( self ):
		self.wordLists = {}
		self.wordLists[ "noun" ] = []
		self.wordLists[ "adjective" ] = []
		self.wordLists[ "transitive_verb" ] = []
		self.wordLists[ "verb" ] = []
		self.wordLists[ "subject" ] = []

		self.LoadLists()
		
	def LoadLists( self ):
		rootFolder = os.path.join( "wordlists", os.path.dirname( os.path.realpath( __file__ ) ) )
		contents = list()

		for root, directories, files in os.walk( rootFolder, topdown = False ):
			for name in files:
				contents.append( os.path.join( root, name ) )

		for item in contents:
			if ( "txt" not in item ):
				continue

			#print( item )

			wordType = "noun"
			if ( "noun" in item ):
				wordType = "noun"
				
			elif ( "adjective" in item ):
				wordType = "adjective"
				
			elif ( "transitive_verbs" in item ):
				wordType = "transitive_verb"
				
			elif ( "verb" in item ):
				wordType = "verb"
				
			elif ( "subject" in item ):
				wordType = "subject"

			with open( os.path.join( rootFolder, item ), "r" ) as infile:
				for line in infile:
					trimmed = line.strip( "\n" )
					self.wordLists[ wordType ].append( trimmed )

		print( "Total words: \n\t" + 
			str( len( self.wordLists["noun"] ) ) + " nouns, " +
			str( len( self.wordLists["adjective"] ) ) + " adjectives, " +
			str( len( self.wordLists["subject"] ) ) + " subjects, " +
			str( len( self.wordLists["verb"] ) ) + " verbs, " +
			str( len( self.wordLists["transitive_verb"] ) ) + " transitive verbs \n" )

	def GetRandomWord( self, wordType ):
		#print( "Word type: " + wordType )
		randIndex = random.randint( 0, len( self.wordLists[ wordType ] ) - 1 )
		return self.wordLists[ wordType ][ randIndex ]

class TweetMaker:
	def __init__( self ):
		self.vocabulary = Vocabulary()
	
	def GetTweet( self ):
		
		randomNoun1 = self.vocabulary.GetRandomWord( "subject" )
		randomNoun2 = self.vocabulary.GetRandomWord( "noun" )
		randomNoun3 = self.vocabulary.GetRandomWord( "noun" )
		
		randomVerbTransitive = self.vocabulary.GetRandomWord( "transitive_verb" )

		statement = random.randint( 0, 1 )
		tweet = "An alternative timeline where "
		#if ( statement == 0 ):
			#tweet = tweet + randomNoun3 + "s " + randomVerbTransitive + " " + randomNoun2 + "s"
		#else:
		tweet = tweet + randomNoun1 + " " + randomVerbTransitive + " " + randomNoun2
		
		return tweet

class TwitterBot:
	def __init__( self ):
		self.api = None
		self.waitMinutes = 60
		self.offline = False
		self.tweetMaker = TweetMaker()
	
	def Initialize( self ):
		if ( self.offline ):
			print( "BOT:\t I'm currently in offline mode." )
			
		else:
			auth = tweepy.OAuthHandler( api_info.consumerKey, api_info.consumerSecret )
			auth.set_access_token( api_info.accessKey, api_info.accessSecret )
			self.api = tweepy.API( auth )

	def Run( self ):
		while( True ):


			print( "Random timeline generator" )
			#for i in range( 10 ):
			tweet = self.tweetMaker.GetTweet()
			print( "\t * " + tweet + "\n" )
			#self.api.update_status( tweet )


			again = raw_input( "Again? (y/n): " )
			if ( again == "n" ):
				break
			# Wait before next tweet
			#time.sleep( 60 * self.waitMinutes )

	def WriteFile( self, fileHandler ):
		for i in range( 1000 ):
			tweet = self.tweetMaker.GetTweet()
			fileHandler.write( tweet + "\n" )

fileHandler = open( "alternative-timelines.txt", "w" );

bot = TwitterBot()
bot.Initialize()
bot.WriteFile( fileHandler )

fileHandler.close()
