from wordlists import Vocabulary
import random

vocab = Vocabulary()
vocab.LoadLists( "esperanto", [ "substantivoj", "adjektivoj", "adverboj", "verboj", "bestoj", "koloroj" ] )

#print( "substantivoj: ",    vocab.GetCount( "substantivoj" ) )
#print( "adjektivoj: ",      vocab.GetCount( "adjektivoj" ) )
#print( "adverboj: ",        vocab.GetCount( "adverboj" ) )
#print( "verboj: ",          vocab.GetCount( "verboj" ) )
#print( "bestoj: ",          vocab.GetCount( "bestoj" ) )

def GenerateName():
    x = random.randint( 4, 9 )
    name = ""

    if ( x == 1 ):
        name = "La " + vocab.GetRandomWord( "adjektivoj" ) + " " + vocab.GetRandomWord( "substantivoj" )
        
    elif ( x == 2 ):
        name = vocab.GetRandomWord( "substantivoj" ) + "-" + vocab.GetRandomWord( "substantivoj" )
        
    elif ( x == 3 ):
        name = vocab.GetRandomWord( "adjektivoj" ) + "-" + vocab.GetRandomWord( "substantivoj" )

    elif ( x == 4 ):
        name = vocab.GetRandomWord( "adjektivoj" ) + "-" + vocab.GetRandomWord( "bestoj" )

    elif ( x == 5 ):
        name = "La " + vocab.GetRandomWord( "koloroj" ) + "-" + vocab.GetRandomWord( "bestoj" )

    elif ( x == 6 ):
        name = vocab.GetRandomWord( "bestoj" ) + "-viro"

    elif ( x == 7 ):
        name = vocab.GetRandomWord( "bestoj" ) + "-virino"

    elif ( x == 8 ):
        name = vocab.GetRandomWord( "adjektivoj" ) + "-viro"

    elif ( x == 9 ):
        name = vocab.GetRandomWord( "adjektivoj" ) + "-virino"

    return name

while ( True ):

    print( "La nomo de viaj Esperantistaj superherooj estas:" )

    for i in range( 3 ):
        n = GenerateName()
        print( "* " + n )
    
    print( "" )
    choice = raw_input( "Denove? (j/n): " )
    if ( choice == "n" ):
        break
