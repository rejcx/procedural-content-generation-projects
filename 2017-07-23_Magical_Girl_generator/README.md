# Notes

Try to generate a "Magical Girl"-like comic or short film.
Similar to the old Sailor Moon anime or Pokemon, the plot is
mostly the same every episode ("Monster-of-the-day"),
so maybe this can be boiled down to something generated
procedurally.

Things that would need to be generated...:

* Characters
    * Names
    * Appearances
        * Poses
        * Clothes
        * Animations
* Plot
    * Basic plot structure
    * Randomly choose the theme of the day, like in Sailor Moon...
        e.g., friend's jewelry store, arcade, etc.
* Script
    * Movement
        * Be able to figure out what kind of movement needs to be
        done, to signal what kind of animations to execute, and where
        to move the characters.
    * Dialogue - Generate things to say in somewhat coherent sentences
    about the current topic relating to the story. ("That monster is BAD.")
* Backgrounds
    * Perhaps tell the computer, "A city backdrop has buildings...", and
    "A building is a rectangle with windows.", and so on, and have
    the computer generate background art.
* Music
    * Music generation for different mood types?
    * Need to research some music theory
* Voice
    * Perhaps have a base set of vocabulary, and record those
    words with multiple actors, then the computer puts it together.
* Compiling
    * Have the program read the generated script and draw, animate,
    and play sounds/music as things run. Record with video capture software.

---

## Magical girl naming ideas

In Sailor Moon, everybody is named after different planets.

Other ideas might be...

* Types of birds
* Types of butterflies
* 
