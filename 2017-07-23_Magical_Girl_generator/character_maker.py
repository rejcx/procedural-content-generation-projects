#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *
import random

pygame.init()
fpsClock = pygame.time.Clock()
window = pygame.display.set_mode( ( 1700, 800 ) )
pygame.display.set_caption( "Rachel's Procedural Crap" )

clrBackground = pygame.Color( 95, 163, 240 )
clrText = pygame.Color( 255, 255, 255 )
font = pygame.font.Font( "Averia-Bold.ttf", 40 )

nameSets = [
    [ "Pasero", "Rubekolo", "Kolombo", "Oriolo", "Kardelo", "Sturno", "Parvolo", "Cigno", "Papago", "Kolibro", "Gruo", "Kukolo" ], # Birds
    [ "Rozo", "Violo", "Lekanto", "Helianto", "Dianto", "Hyacinto", "Lilio", "Antirino", "Tulipo" ], # Flowers
]

titles = [ "Kavaliro", "Magia", "Luma", "Hela", "Gardisto", "Miraklo", "Sonĝo", "Kuraĝa", "Reva" ]

title   = random.randint( 0, len( titles ) - 1 )
nameSet = random.randint( 0, len( nameSets ) - 1 )

print( "Title:      " + str( title ) )
print( "Name set:   " + str( nameSet ) )

class Character:
    def __init__( self ):
        self.x = 0
        self.y = 0
        self.name = ""
        
        self.imgBase         = None
        self.imgHairBack     = None
        self.imgHairFront    = None
        self.imgClothes      = None
        self.imgFace         = None

        self.labelObj        = None

        self.GenerateNewAppearance()
        self.GenerateNewName()

    def GenerateNewAppearance( self ):
        self.base = str( random.randint( 1, 12 ) )
        self.clothes = str( random.randint( 1, 5 ) )
        self.hairA = str( random.randint( 1, 5 ) )
        self.hairB = str( random.randint( 1, 14 ) )
        self.face = str( random.randint( 1, 6 ) )

        print( "Base:       " + self.base )
        print( "Clothes:    " + self.clothes )
        print( "Hair A:     " + self.hairA )
        print( "Hair B:     " + self.hairB )
        
        if ( self.hairA != "2" and self.hairA != "4" and self.hairA != "5" ):
            self.imgHairBack     = pygame.image.load( "character_assets/hair_" + self.hairA + "_" + self.hairB + ".png" )
        else:
            self.imgHairBack = None
            
        self.imgBase         = pygame.image.load( "character_assets/base_" + self.base + ".png" )
        self.imgHairFront    = pygame.image.load( "character_assets/hairf_" + self.hairA + "_" + self.hairB + ".png" )
        self.imgClothes      = pygame.image.load( "character_assets/outfit_" + self.clothes + ".png" )
        self.imgFace         = pygame.image.load( "character_assets/face_" + self.face + ".png" )

    def GenerateNewName( self ):
        global clrText
        global font
        global nameSets
        global titles
        global nameSet
        global title

        name = random.randint( 0, len( nameSets[ nameSet ] ) - 1 )
        
        self.name = titles[ title ] + " " + nameSets[ nameSet ][ name ]
        self.labelObj = font.render( self.name, False, clrText )

    def Draw( self, window ):
        if ( self.imgHairBack != None ):
            window.blit( self.imgHairBack, ( self.x, self.y ) )
        window.blit( self.imgBase, ( self.x, self.y ) )
        window.blit( self.imgHairFront, ( self.x, self.y ) )
        window.blit( self.imgClothes, ( self.x, self.y ) )
        window.blit( self.imgFace, ( self.x, self.y ) )
        
        window.blit( self.labelObj, ( self.x + 200, self.y + 600 ) )

    def SetPosition( self, x, y ):
        self.x = x
        self.y = y



a = Character()
b = Character()
c = Character()

a.SetPosition( 0, 0 )
b.SetPosition( 500, 0 )
c.SetPosition( 1000, 0 )

while True:
    window.fill( clrBackground )
    a.Draw( window )
    b.Draw( window )
    c.Draw( window )

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

        elif event.type == KEYDOWN:
            if event.key == K_a:
                a.GenerateNewAppearance()
            elif event.key == K_s:
                b.GenerateNewAppearance()
            elif event.key == K_d:
                c.GenerateNewAppearance()
                
            if event.key == K_z:
                a.GenerateNewName()
            elif event.key == K_x:
                b.GenerateNewName()
            elif event.key == K_c:
                c.GenerateNewName()

            if event.key == K_q:
                title   = random.randint( 0, len( titles ) - 1 )
                nameSet = random.randint( 0, len( nameSets ) - 1 )
                a.GenerateNewName()
                b.GenerateNewName()
                c.GenerateNewName()

    pygame.display.update()
    fpsClock.tick( 30 )
