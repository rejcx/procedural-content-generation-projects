import pygame, sys
from pygame.locals import *

class AnimatedCharacter:
    def __init__( self ):
        self.x = 300
        self.y = 100

        self.speed = 0.1
        self.frame = 0
        self.animation = "walk_left"

        self.drawOrder = [
            "upper_left_arm",
            "lower_left_arm",
            "left_hand",
            "upper_left_leg",
            "lower_left_leg",
            "left_foot",
            "head",
            "torso",
            "upper_right_arm",
            "lower_right_arm",
            "right_hand",
            "upper_right_leg",
            "lower_right_leg",
            "right_foot",
        ]

        self.animations = {}

        self.bodyparts = {}
        
        self.bodyparts["head"]              = { "relx" : 0,     "rely" : 0 }
        self.bodyparts["torso"]             = { "relx" : 12,    "rely" : 150 }
        
        self.bodyparts["upper_left_arm"]    = { "relx" : 0,     "rely" : 150 }
        self.bodyparts["upper_right_arm"]   = { "relx" : 106,   "rely" : 150 }
        self.bodyparts["lower_left_arm"]    = { "relx" : 0,     "rely" : 210 }
        self.bodyparts["lower_right_arm"]   = { "relx" : 106,   "rely" : 210 }
        self.bodyparts["left_hand"]         = { "relx" : 0,     "rely" : 270 }
        self.bodyparts["right_hand"]        = { "relx" : 106,   "rely" : 270 }

        self.bodyparts["upper_left_leg"]    = { "relx" : 15,    "rely" : 360 }
        self.bodyparts["upper_right_leg"]   = { "relx" : 91,    "rely" : 360 }
        self.bodyparts["lower_left_leg"]    = { "relx" : 15,    "rely" : 440 }
        self.bodyparts["lower_right_leg"]   = { "relx" : 91,    "rely" : 440 }
        self.bodyparts["left_foot"]         = { "relx" : -10,   "rely" : 530 }
        self.bodyparts["right_foot"]        = { "relx" : 91,    "rely" : 530 }

        self.SetupAnimationFrame()

    def SetupAnimationFrame( self ):
        self.animations["stand_forward"] = [
            {
                "head"              : { "relx" : 0,     "rely" : 0,       "rotation" : 0 },
                "torso"             : { "relx" : 12,    "rely" : 150,     "rotation" : 0 },
                "upper_left_arm"    : { "relx" : 0,     "rely" : 150,     "rotation" : 0 },
                "upper_right_arm"   : { "relx" : 106,   "rely" : 150,     "rotation" : 0 },
                "lower_left_arm"    : { "relx" : 0,     "rely" : 210,     "rotation" : 0 },
                "lower_right_arm"   : { "relx" : 106,   "rely" : 210,     "rotation" : 0 },
                "left_hand"         : { "relx" : 0,     "rely" : 270,     "rotation" : 0 },
                "right_hand"        : { "relx" : 106,   "rely" : 270,     "rotation" : 0 },
                "upper_left_leg"    : { "relx" : 15,    "rely" : 360,     "rotation" : 0 },
                "upper_right_leg"   : { "relx" : 91,    "rely" : 360,     "rotation" : 0 },
                "lower_left_leg"    : { "relx" : 15,    "rely" : 440,     "rotation" : 0 },
                "lower_right_leg"   : { "relx" : 91,    "rely" : 440,     "rotation" : 0 },
                "left_foot"         : { "relx" : -10,   "rely" : 530,     "rotation" : 0 },
                "right_foot"        : { "relx" : 91,    "rely" : 530,     "rotation" : 0 },
            }
        ]
        
        self.animations["walk_left"] = [
            { #Middle
                "head"              : { "relx" : 0,     "rely" : 0,       "rotation" : 0 },
                "torso"             : { "relx" : 12,    "rely" : 150,     "rotation" : 0 },
                
                "upper_left_arm"    : { "relx" : 50,    "rely" : 150,     "rotation" : 0 },
                "lower_left_arm"    : { "relx" : 50,    "rely" : 210,     "rotation" : 0 },
                "left_hand"         : { "relx" : 50,    "rely" : 270,     "rotation" : 0 },
                
                "upper_right_arm"   : { "relx" : 50+5,    "rely" : 150,     "rotation" : 0 },
                "lower_right_arm"   : { "relx" : 50+5,    "rely" : 210,     "rotation" : 0 },
                "right_hand"        : { "relx" : 50+5,    "rely" : 270,     "rotation" : 0 },
                
                "upper_right_leg"   : { "relx" : 50,    "rely" : 360-5,     "rotation" : 0 },
                "lower_right_leg"   : { "relx" : 50,    "rely" : 440-5,     "rotation" : 0 },
                "right_foot"        : { "relx" : 30,    "rely" : 530-5,     "rotation" : 0 },
                
                "upper_left_leg"    : { "relx" : 50,    "rely" : 360-5,     "rotation" : 0 },
                "lower_left_leg"    : { "relx" : 50,    "rely" : 440-5,     "rotation" : 0 },
                "left_foot"         : { "relx" : 30,    "rely" : 530-5,     "rotation" : 0 },
            },
            { 
                "head"              : { "relx" : 0,         "rely" : 0+10,         "rotation" : 0 },
                "torso"             : { "relx" : 12,        "rely" : 150+10,       "rotation" : 0 },
                
                "upper_left_arm"    : { "relx" : 50+10,    "rely" : 150+10,    "rotation" : 0 },
                "lower_left_arm"    : { "relx" : 50+10,    "rely" : 210+10,    "rotation" : 0 },
                "left_hand"         : { "relx" : 50+10,    "rely" : 270+10,    "rotation" : 0 },
                
                "upper_right_arm"   : { "relx" : 50-10,    "rely" : 150+10,    "rotation" : -15 },
                "lower_right_arm"   : { "relx" : 50-40,    "rely" : 210+5,    "rotation" : -25 },
                "right_hand"        : { "relx" : 50-45,    "rely" : 270+0,    "rotation" : 0 },
                
                "upper_right_leg"   : { "relx" : 50+5+5,     "rely" : 360+5,    "rotation" : 15 },
                "lower_right_leg"   : { "relx" : 50+27+5,    "rely" : 440+0,    "rotation" : 25 },
                "right_foot"        : { "relx" : 30+55+5,    "rely" : 530-15,    "rotation" : 25 },
                #out
                "upper_left_leg"    : { "relx" : 50-15-5,    "rely" : 360+5,    "rotation" : -15 },
                "lower_left_leg"    : { "relx" : 50-15-5,    "rely" : 440+5,    "rotation" : -0 },
                "left_foot"         : { "relx" : 30-20-5,    "rely" : 530-0,    "rotation" : -0 },
            },
            { #Middle
                "head"              : { "relx" : 0,     "rely" : 0,       "rotation" : 0 },
                "torso"             : { "relx" : 12,    "rely" : 150,     "rotation" : 0 },
                
                "upper_left_arm"    : { "relx" : 50,    "rely" : 150,     "rotation" : 0 },
                "lower_left_arm"    : { "relx" : 50,    "rely" : 210,     "rotation" : 0 },
                "left_hand"         : { "relx" : 50,    "rely" : 270,     "rotation" : 0 },
                
                "upper_right_arm"   : { "relx" : 50+5,    "rely" : 150,     "rotation" : 0 },
                "lower_right_arm"   : { "relx" : 50+5,    "rely" : 210,     "rotation" : 0 },
                "right_hand"        : { "relx" : 50+5,    "rely" : 270,     "rotation" : 0 },
                
                "upper_right_leg"   : { "relx" : 50,    "rely" : 360-5,     "rotation" : 0 },
                "lower_right_leg"   : { "relx" : 50,    "rely" : 440-5,     "rotation" : 0 },
                "right_foot"        : { "relx" : 30,    "rely" : 530-5,     "rotation" : 0 },
                
                "upper_left_leg"    : { "relx" : 50,    "rely" : 360-5,     "rotation" : 0 },
                "lower_left_leg"    : { "relx" : 50,    "rely" : 440-5,     "rotation" : 0 },
                "left_foot"         : { "relx" : 30,    "rely" : 530-5,     "rotation" : 0 },
            },
            {
                "head"              : { "relx" : 0,         "rely" : 0+10,         "rotation" : 0 },
                "torso"             : { "relx" : 12,        "rely" : 150+10,       "rotation" : 0 },
                
                "upper_left_arm"    : { "relx" : 50-10,    "rely" : 150+10,    "rotation" : -15 },
                "lower_left_arm"    : { "relx" : 50-30,    "rely" : 210+10,    "rotation" : -15 },
                "left_hand"         : { "relx" : 50-35,    "rely" : 270+10,    "rotation" : 0 },
                
                "upper_right_arm"   : { "relx" : 50+10,    "rely" : 150+10,    "rotation" : 15 },
                "lower_right_arm"   : { "relx" : 50+26,    "rely" : 210+9,    "rotation" : 15 },
                "right_hand"        : { "relx" : 50+45,    "rely" : 270+10,    "rotation" : 0 },
                #out
                "upper_right_leg"   : { "relx" : 50-15-5,    "rely" : 360+5,    "rotation" : -15 },
                "lower_right_leg"   : { "relx" : 50-15-5,    "rely" : 440+5,    "rotation" : -0 },
                "right_foot"        : { "relx" : 30-20-5,    "rely" : 530-0,    "rotation" : -0 },
                
                "upper_left_leg"    : { "relx" : 50+5+5,     "rely" : 360+5,    "rotation" : 15 },
                "lower_left_leg"    : { "relx" : 50+27+5,    "rely" : 440+0,    "rotation" : 25 },
                "left_foot"         : { "relx" : 30+55+5,    "rely" : 530-15,    "rotation" : 25 },
            },
        ]

    def SetPartPath( self, paths ):
        self.bodyparts["head"]["image"]                = pygame.image.load( paths[ "head" ] )
        self.bodyparts["torso"]["image"]               = pygame.image.load( paths[ "torso" ] )
        self.bodyparts["upper_left_arm"]["image"]      = pygame.image.load( paths[ "upper_left_arm" ] )
        self.bodyparts["upper_right_arm"]["image"]     = pygame.image.load( paths[ "upper_right_arm" ] )
        self.bodyparts["lower_left_arm"]["image"]      = pygame.image.load( paths[ "lower_left_arm" ] )
        self.bodyparts["lower_right_arm"]["image"]     = pygame.image.load( paths[ "lower_right_arm" ] )
        self.bodyparts["left_hand"]["image"]           = pygame.image.load( paths[ "left_hand" ] )
        self.bodyparts["right_hand"]["image"]          = pygame.image.load( paths[ "right_hand" ] )
        self.bodyparts["upper_left_leg"]["image"]      = pygame.image.load( paths[ "upper_left_leg" ] )
        self.bodyparts["upper_right_leg"]["image"]     = pygame.image.load( paths[ "upper_right_leg" ] )
        self.bodyparts["lower_left_leg"]["image"]      = pygame.image.load( paths[ "lower_left_leg" ] )
        self.bodyparts["lower_right_leg"]["image"]     = pygame.image.load( paths[ "lower_right_leg" ] )
        self.bodyparts["left_foot"]["image"]           = pygame.image.load( paths[ "left_foot" ] )
        self.bodyparts["right_foot"]["image"]          = pygame.image.load( paths[ "right_foot" ] )

    def Draw( self, window ):
        for i in self.drawOrder:
            key = i
            surf = pygame.transform.rotate( self.bodyparts[key]["image"], self.animations[ self.animation ][ int( self.frame ) ][ key ][ "rotation" ] )
            x = self.x + self.animations[ self.animation ][ int( self.frame ) ][ key ][ "relx" ]
            y = self.y + self.animations[ self.animation ][ int( self.frame ) ][ key ][ "rely" ]
            window.blit( surf, (x, y) )
            
        #for key, part in self.bodyparts.iteritems():


    def Update( self ):
        self.frame = self.frame + self.speed
        if ( self.frame >= len( self.animations[ self.animation ] ) ):
            self.frame = 0





char = AnimatedCharacter()
char.SetPartPath( {
    "head"              : "images/head.png",
    "torso"             : "images/torso.png",
    "upper_left_arm"    : "images/upper_left_arm.png",
    "upper_right_arm"   : "images/upper_right_arm.png",
    "lower_left_arm"    : "images/lower_left_arm.png",
    "lower_right_arm"   : "images/lower_right_arm.png",
    "left_hand"         : "images/left_hand.png",
    "right_hand"        : "images/right_hand.png",
    "upper_left_leg"    : "images/upper_left_leg.png",
    "upper_right_leg"   : "images/upper_right_leg.png",
    "lower_left_leg"    : "images/lower_left_leg.png",
    "lower_right_leg"   : "images/lower_right_leg.png",
    "left_foot"         : "images/left_foot.png",
    "right_foot"        : "images/right_foot.png",
} )



pygame.init()
fpsClock = pygame.time.Clock()
window = pygame.display.set_mode( ( 800, 800 ) )
pygame.display.set_caption( "Rachel's Animator Thingy" )

clrBackground = pygame.Color( 95, 163, 240 )

while True:
    window.fill( clrBackground )
    char.Draw( window )

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    char.Update()

    pygame.display.update()
    fpsClock.tick( 30 )
